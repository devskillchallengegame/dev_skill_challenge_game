package pl.base.application.ui.screens.settingsScreen;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.nononsenseapps.filepicker.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.base.application.cloud.questions.QuestionCreator;
import pl.base.application.cloud.users.UserUpdater;
import pl.base.application.models.FirebaseQuestion;
import pl.base.application.ui.BasePresenter;
import pl.base.application.utils.FirebaseQuestionParser;
import pl.base.basic.R;

import static pl.base.application.utils.MessageBarsUtil.showToast;

class SettingsActivityPresenter implements BasePresenter<SettingsActivity> {

    private SettingsActivity activity;
    private File questionsToParse;

    @Override
    public void onLoad(SettingsActivity activity) {
        this.activity = activity;
    }

    void changeNick(String newNick) {
        new UserUpdater(isSuccess -> {
            if (isSuccess) {
                activity.getLayout().progressBar.setVisibility(View.GONE);
                activity.setDisplayingUserName();
                showToast(activity, R.string.settings_activity_username_changed_successfully);
            } else {
                showToast(activity, R.string.settings_activity_username_changed_failure);
            }
        }).update(newNick);
    }

    void handleFilePick(Intent intent) {
        List<Uri> files = Utils.getSelectedFilesFromResult(intent);
        for (Uri uri : files) {
            questionsToParse = Utils.getFileForUri(uri);
        }
    }

    void sendQuestions() {
        if (questionsToParse != null) {
            try {
                ArrayList<FirebaseQuestion> questions = parseQuestions();
                QuestionCreator questionCreator = new QuestionCreator();
                for (FirebaseQuestion question : questions) {
                    questionCreator.create(question);
                }
                activity.getLayout().tvQuestionsImportStatus.setText("Sent");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            activity.getLayout().tvQuestionsImportStatus.setText("Import questions first");
        }
    }

    private ArrayList<FirebaseQuestion> parseQuestions() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(questionsToParse));
        FirebaseQuestionParser firebaseQuestionParser = new FirebaseQuestionParser();
        ArrayList<FirebaseQuestion> questions = new ArrayList<>();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            questions.add(firebaseQuestionParser.parseQuestion(line));
        }
        return questions;
    }

}
