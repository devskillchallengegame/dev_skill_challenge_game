package pl.base.application.ui.screens.questionScreen;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import pl.base.application.utils.TouchableButton;
import pl.base.basic.R;

class ViewHolder {

    ProgressBar pbTimeCollapsing;
    TextView tvQuestionNumber, tvQuestionsCount, tvQuestionText;
    TouchableButton tBtnAnswer1, tBtnAnswer2, tBtnAnswer3, tBtnAnswer4;
    ImageView ivArrowNextQuestion;

    public ViewHolder(View view) {
        pbTimeCollapsing = view.findViewById(R.id.activity_question_pb_time_collapsing);
        tvQuestionNumber = view.findViewById(R.id.activity_question_tv_question_number);
        tvQuestionsCount = view.findViewById(R.id.activity_question_tv_questions_count);
        tvQuestionText = view.findViewById(R.id.activity_question_tv_question_text);
        tBtnAnswer1 = view.findViewById(R.id.activity_question_btn_answer1);
        tBtnAnswer2 = view.findViewById(R.id.activity_question_btn_answer2);
        tBtnAnswer3 = view.findViewById(R.id.activity_question_btn_answer3);
        tBtnAnswer4 = view.findViewById(R.id.activity_question_btn_answer4);
        ivArrowNextQuestion = view.findViewById(R.id.activity_question_iv_arrow_next);
    }
}
