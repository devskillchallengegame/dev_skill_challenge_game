package pl.base.application.ui.screens.waitingRoomScreen;

import android.view.View;

import pl.base.application.cloud.rooms.RoomServant;
import pl.base.application.cloud.rooms.RoomUpdater;
import pl.base.application.models.Room;
import pl.base.application.ui.BasePresenter;
import pl.base.basic.R;
import pl.base.firebase_engine.auth.AuthManager;

import static pl.base.application.models.RoomStatus.DURING;
import static pl.base.application.utils.MessageBarsUtil.showToast;

class WaitingRoomActivityPresenter implements BasePresenter<WaitingRoomActivity> {

    private WaitingRoomActivity activity;
    private RoomServant roomServant;

    @Override
    public void onLoad(WaitingRoomActivity activity) {
        this.activity = activity;
        getRoom();
    }

    private void getRoom() {
        Room room = activity.getRoom();
        String roomId = room.getId();
        roomServant = new RoomServant();
        roomServant.getRoom(roomId, new OnGetRoomListenerImpl(activity));
    }

    void setButtonVisibility() {
        String userId = AuthManager.getInstance().getUserId();
        Room room = activity.getRoom();
        if (room.getOwnerId().equals(userId)) {
            activity.getLayout().btnStart.setVisibility(View.VISIBLE);
        }
    }

    void unregisterGetRoomListener() {
        if (roomServant != null) {
            roomServant.unregisterGetRoomListener();
        }
    }

    void startGame() {
        if (getMembersCount() >= 1) {
            changeRoomStatus();
        } else {
            showToast(activity, R.string.waiting_room_activity_no_members_error_msg);
        }
    }

    private void changeRoomStatus() {
        Room room = activity.getRoom();
        RoomUpdater roomUpdater = new RoomUpdater(room.getId());
        roomUpdater.changeRoomStatus(DURING, isSuccess -> {
            if (isSuccess) {
                unregisterGetRoomListener();
                activity.showQuestionsScreen();
            }
        });
    }

    private int getMembersCount() {
        return activity.getRoom().getMembers().size();
    }

}