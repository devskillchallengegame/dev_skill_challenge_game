package pl.base.application.ui.screens.createRoomScreen;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.Question;
import pl.base.application.sheets.ResultCallback;
import pl.base.application.utils.SheetQuestionParser;
import pl.base.basic.R;

import static pl.base.application.utils.MessageBarsUtil.showToast;

@AllArgsConstructor
public class OnGetQuestionsListenerImpl implements OnGetObjectListener<ArrayList<Question>>, ResultCallback {

    private CreateRoomActivity activity;

    //for spreadsheet request only
    @Override
    public void onResult(List<String> response) {
        CreateRoomActivityPresenter presenter = activity.getPresenter();
        SheetQuestionParser parser = new SheetQuestionParser();
        ArrayList<Question> parsedQuestions = new ArrayList<>();
        for (String rawQuestion : response) {
            parsedQuestions.add(parser.parseQuestion(rawQuestion));
        }
        presenter.createRoom(parsedQuestions);
    }

    @Override
    public void onSuccess(ArrayList<Question> questions) {
        if (!questions.isEmpty()) {
            CreateRoomActivityPresenter presenter = activity.getPresenter();
            ArrayList<Question> randomizedQuestions = presenter.randomizeQuestions(questions);
            presenter.createRoom(randomizedQuestions);
        } else {
            showToast(activity, R.string.create_room_activity_room_no_questions_error);
        }
    }

    @Override
    public void onFailure(ErrorStatus errorStatus) {

    }

    @Override
    public void onEnd() {

    }
}