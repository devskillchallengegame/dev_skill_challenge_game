package pl.base.application.ui.screens.rankingScreen.rankingAdapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.base.application.models.User;
import pl.base.application.ui.generalListeners.OnItemClickedListener;
import pl.base.basic.R;

public class UserAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<User> usersList;
    private OnItemClickedListener onItemClickedListener;
    private ViewHolder layout;
    private User user;

    public UserAdapter(ArrayList<User> usersList, OnItemClickedListener onItemClickedListener) {
        this.usersList = usersList;
        this.onItemClickedListener = onItemClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_ranking_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder layout, int position) {
        this.layout = layout;
        this.user = usersList.get(position);
        initView(position + 1);
        //setListeners(position);
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    private void initView(int position) {
        layout.tvPosition.setText(String.valueOf(position) + ".");
        layout.tvUserName.setText(user.getNick());
        layout.tvPoints.setText(String.valueOf(user.getPoints()));
        if (position == 1) {
            layout.ivLeader.setVisibility(View.VISIBLE);
            layout.tvPosition.setTextColor(Color.GREEN);
            layout.tvUserName.setTextColor(Color.GREEN);
        }
    }

    private void setListeners() {
        //layout.itemView.setOnClickListener(v -> onItemClickedListener.onClicked(position));
    }


}
