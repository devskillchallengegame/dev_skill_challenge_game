package pl.base.application.ui.screens.settingsScreen.dialogs;

import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import lombok.AllArgsConstructor;
import pl.base.application.ui.generalListeners.DialogCallbackListener;

@AllArgsConstructor
public class DialogsBtnClickImplSettings implements MaterialDialog.SingleButtonCallback {

    private SettingsActivityDialogInfo dialogsInfo;
    private DialogCallbackListener listener;

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction action) {
        switch (dialogsInfo) {
            case CHANGE_NICK:
                listener.onCallback(dialog, action);
                break;
        }
    }
}
