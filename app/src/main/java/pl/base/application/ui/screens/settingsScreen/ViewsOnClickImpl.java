package pl.base.application.ui.screens.settingsScreen;

import android.view.View;

import lombok.AllArgsConstructor;
import pl.base.application.utils.OnOneClickListener;
import pl.base.application.utils.SoundsUtils;

import static pl.base.application.configs.PrefsConfig.SOUND_PREFS_KEY;
import static pl.base.application.core.MyApplication.getPrefsCore;

@AllArgsConstructor
class ViewsOnClickImpl extends OnOneClickListener {

    private SettingsActivity activity;

    @Override
    public void onOneClick(View view) {
        ViewHolder layout = activity.getLayout();
        int viewId = view.getId();
        if (viewId == layout.btnChange.getId()) {
            activity.showNickChangeDialog();
        } else if (viewId == layout.btnImportQuestions.getId()) {
            activity.askForReadFilesPermission();
        } else if (viewId == layout.btnSendQuestions.getId()) {
            activity.getPresenter().sendQuestions();
        } else if (viewId == layout.cbSound.getId()) {
            boolean isSoundOn = layout.cbSound.isChecked();
            getPrefsCore().putValue(SOUND_PREFS_KEY, isSoundOn);
            if (!isSoundOn) {
                SoundsUtils.stopMusic();
            }
        }

    }
}
