package pl.base.application.ui.screens.summaryScreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import lombok.Getter;
import pl.base.application.models.Room;
import pl.base.basic.R;

import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;

public class SummaryActivity extends AppCompatActivity {

    public static final String SUMMARY_ACTIVITY_ROOM_EXTRAS_KEY = "summary_activity_room_extras_key";
    @Getter
    private SummaryActivityPresenter presenter;
    @Getter
    private ViewHolder layout;
    @Getter
    private Room room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        setPresenter();
    }

    private void initView() {
        setContentView(R.layout.activity_room_summary);

        getWindow().addFlags(FLAG_KEEP_SCREEN_ON);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        View content = findViewById(android.R.id.content);

        layout = new ViewHolder(content);
        layout.getRecyclerView().setLayoutManager(layoutManager);
        layout.getRecyclerView().setHasFixedSize(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            room = (Room) bundle.getSerializable(SUMMARY_ACTIVITY_ROOM_EXTRAS_KEY);
        }

        setListeners();
    }


    private void setPresenter() {
        presenter = new SummaryActivityPresenter();
        presenter.onLoad(this);
    }

    private void setListeners() {
        ViewsOnClickImpl onClickListener = new ViewsOnClickImpl(this);
        layout.btnFinish.setOnClickListener(onClickListener);
    }

}