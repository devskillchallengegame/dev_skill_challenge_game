package pl.base.application.ui.screens.roomsListScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import lombok.Getter;
import pl.base.application.models.Room;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.roomsListScreen.roomAdapter.RoomAdapter;
import pl.base.application.ui.screens.waitingRoomScreen.WaitingRoomActivity;
import pl.base.application.utils.MessageBarsUtil;
import pl.base.basic.R;

import static android.support.v7.widget.RecyclerView.LayoutManager;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static pl.base.application.ui.screens.waitingRoomScreen.WaitingRoomActivity.WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY;

public class RoomsListActivity extends BaseActivity {

    @Getter
    private RoomsListActivityPresenter presenter;
    @Getter
    private ViewHolder layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        setPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getRooms();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unregisterGetRoomsListener();
    }

    public void showRooms(RoomAdapter adapter) {
        layout.recyclerView.setAdapter(adapter);
        layout.recyclerView.setVisibility(VISIBLE);
    }

    public void hideRooms() {
        layout.recyclerView.setVisibility(GONE);
    }

    public void showProgressBar() {
        layout.progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        layout.progressBar.setVisibility(GONE);
    }

    public void showGetRoomsError() {
        MessageBarsUtil.showToast(this, R.string.rooms_list_activity_get_rooms_error);
    }

    private void initView() {
        setContentView(R.layout.activity_rooms_list);
        View content = findViewById(android.R.id.content);
        LayoutManager layoutManager = new LinearLayoutManager(this);
        layout = new ViewHolder(content);
        layout.recyclerView.setLayoutManager(layoutManager);
        layout.recyclerView.setHasFixedSize(true);
        setListeners();
    }

    private void setListeners() {
        layout.swipeRefreshLayout.setOnRefreshListener(new OnRefreshListenerImpl(this));
    }

    private void setPresenter() {
        presenter = new RoomsListActivityPresenter();
        presenter.onLoad(this);
    }

    void startWaitingRoomScreen(Room room) {
        Intent intent = new Intent(this, WaitingRoomActivity.class);
        intent.putExtra(WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY, room);
        startActivity(intent);
    }

}
