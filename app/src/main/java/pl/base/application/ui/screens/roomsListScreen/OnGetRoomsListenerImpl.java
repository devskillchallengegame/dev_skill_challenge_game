package pl.base.application.ui.screens.roomsListScreen;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.cloud.rooms.RoomDestructor;
import pl.base.application.models.Room;
import pl.base.application.models.RoomMember;
import pl.base.application.ui.generalListeners.OnItemClickedListener;
import pl.base.application.ui.screens.roomsListScreen.roomAdapter.RoomAdapter;
import pl.base.firebase_engine.auth.AuthManager;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.models.RoomStatus.NEW;

@AllArgsConstructor
class OnGetRoomsListenerImpl implements OnGetObjectListener<ArrayList<Room>> {

    private RoomsListActivity activity;
    private OnItemClickedListener onItemClickedListener;

    @Override
    public void onSuccess(ArrayList<Room> rooms) {
        activity.getLayout().tvEmptyRoomsList.setVisibility(GONE);
        deleteDeprecatedRooms(rooms);
//        ArrayList<Room> roomsWithoutDuplicates = removeDuplicates(rooms);
        ArrayList<Room> filteredRooms = filterPackableRooms(rooms);
        RoomAdapter roomAdapter = new RoomAdapter(filteredRooms, onItemClickedListener);
        activity.getPresenter().setLocalRoomsList(filteredRooms);
        activity.showRooms(roomAdapter);
        if (filteredRooms.isEmpty()) {
            activity.getLayout().tvEmptyRoomsList.setVisibility(VISIBLE);
            activity.hideRooms();
        }
    }

    @Override
    public void onFailure(ErrorStatus errorStatus) {
        activity.showGetRoomsError();
    }

    @Override
    public void onEnd() {
        activity.getLayout().swipeRefreshLayout.setRefreshing(false);
        activity.hideProgressBar();
    }

    private void deleteDeprecatedRooms(ArrayList<Room> rooms) {
        ArrayList<String> idsRoomsToDelete = new ArrayList<>();
        for (Room room : rooms) {
            if (isRoomDeprecated(room) || isUserOwningRoom(room)) {
                idsRoomsToDelete.add(room.getId());
            }
        }
        RoomDestructor roomDestructor = new RoomDestructor();
        roomDestructor.delete(idsRoomsToDelete, null);
    }

    private boolean isRoomDeprecated(Room room) {
        Long currentTime = Calendar.getInstance().getTimeInMillis();
        return currentTime - room.getTimeCreated() > getGameConfig().getRoomValidTime();
    }

    private boolean isUserOwningRoom(Room room) {
        String userId = AuthManager.getInstance().getUserId();
        return room.getOwnerId().equals(userId);
    }

//    private ArrayList<Room> removeDuplicates(ArrayList<Room> rooms) {
//        Set<String> attributes = new HashSet<>();
//        ArrayList<Room> duplicates = new ArrayList<>();
//        for (Room room : rooms) {
//            if (attributes.contains(room.getOwnerId())) {
//                duplicates.add(room);
//            }
//            attributes.add(room.getOwnerId());
//        }
//        rooms.removeAll(duplicates);
//        return rooms;
//    }

    private ArrayList<Room> filterPackableRooms(ArrayList<Room> rooms) {
        ArrayList<Room> filteredRooms = new ArrayList<>();
        for (Room room : rooms) {
            HashMap<String, RoomMember> roomMembers = room.getMembers();
            if (room.getStatus().equals(NEW) && roomMembers != null) {
                Integer membersCount = roomMembers.size();
                if (membersCount < room.getMaxMembers()) {
                    filteredRooms.add(room);
                }
            }
        }
        return filteredRooms;
    }

}
