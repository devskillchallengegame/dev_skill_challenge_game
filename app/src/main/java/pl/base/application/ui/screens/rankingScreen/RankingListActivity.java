package pl.base.application.ui.screens.rankingScreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import lombok.Getter;
import pl.base.application.models.User;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.rankingScreen.rankingAdapter.UserAdapter;
import pl.base.application.utils.MessageBarsUtil;
import pl.base.basic.R;

import static android.support.v7.widget.RecyclerView.LayoutManager;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RankingListActivity extends BaseActivity {

    @Getter
    private RankingListActivityPresenter presenter;
    @Getter
    private ViewHolder layout;
    private User user;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        //getExtras();
        setPresenter();

    }

    public void showUsers(UserAdapter adapter) {
        layout.recyclerView.setAdapter(adapter);
        layout.recyclerView.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        layout.progressBar.setVisibility(GONE);
    }

    public void showGetRankingError() {
        MessageBarsUtil.showToast(this, R.string.ranking_list_activiy_get_users_error);
    }

    private void initView() {
        setContentView(R.layout.activity_ranking);
        View content = findViewById(android.R.id.content);
        LayoutManager layoutManager = new LinearLayoutManager(this);
        layout = new ViewHolder(content);
        layout.getRecyclerView().setLayoutManager(layoutManager);
        layout.getRecyclerView().setHasFixedSize(true);
        setListeners();
    }

    private void setPresenter() {
        presenter = new RankingListActivityPresenter();
        presenter.onLoad(this);
    }

    private void setListeners() {
        layout.swipeRefreshLayout.setOnRefreshListener(new OnRefreshListenerImpl(this));
    }

}
