package pl.base.application.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import pl.base.application.services.NetworkReceiver;
import pl.base.application.ui.screens.mainScreen.MainActivity;
import pl.base.application.ui.screens.questionScreen.QuestionActivity;
import pl.base.basic.R;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;
import static pl.base.application.utils.SoundsUtils.isSoundOn;
import static pl.base.application.utils.SoundsUtils.playBackgroundMusic;
import static pl.base.application.utils.SoundsUtils.playChallengeMusic;
import static pl.base.application.utils.SoundsUtils.stopBackgroundMusic;
import static pl.base.application.utils.SoundsUtils.stopChallangeMusic;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    private NetworkReceiver networkReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareMusic();
        if (!(this instanceof MainActivity)) {
            registerNetworkStateReceiver();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterNetworkStateReceiver();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public void startMainActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.rotation, R.anim.scale_and_rotate_enterance);
    }
//
//    private void registerNetworkStateReceiver() {
//        NetworkReceiver networkReceiver = new NetworkReceiver();
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        this.registerReceiver(networkReceiver, filter);
//    }

    private void prepareMusic() {
        if (isSoundOn()) {
            if (!(this instanceof QuestionActivity)) {
                stopChallangeMusic();
                playBackgroundMusic();
            } else {
                stopBackgroundMusic();
                playChallengeMusic();
            }
        }
    }

    private void registerNetworkStateReceiver() {
        IntentFilter filter = new IntentFilter(CONNECTIVITY_ACTION);
        networkReceiver = new NetworkReceiver();
        this.registerReceiver(networkReceiver, filter);
    }

    private void unRegisterNetworkStateReceiver() {
        if (networkReceiver != null) {
            this.unregisterReceiver(networkReceiver);
        }
    }

}
