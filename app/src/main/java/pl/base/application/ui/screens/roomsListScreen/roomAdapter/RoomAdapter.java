package pl.base.application.ui.screens.roomsListScreen.roomAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.base.application.models.Room;
import pl.base.application.ui.generalListeners.OnItemClickedListener;
import pl.base.application.utils.OnOneClickListener;
import pl.base.basic.R;

public class RoomAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<Room> roomsList;
    private OnItemClickedListener onItemClickedListener;
    private ViewHolder layout;
    private Room room;

    public RoomAdapter(ArrayList<Room> roomsList, OnItemClickedListener onItemClickedListener) {
        this.roomsList = roomsList;
        this.onItemClickedListener = onItemClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder layout, int position) {
        this.layout = layout;
        this.room = roomsList.get(position);
        initView();
        setListeners(position);
    }

    @Override
    public int getItemCount() {
        return roomsList.size();
    }

    private void initView() {
        int membersCount = room.getMembers().size();
        layout.tvRoomName.setText(room.getName());
        layout.tvMembersCount.setText(String.valueOf(membersCount));
        layout.tvMembersMax.setText(String.valueOf(room.getMaxMembers()));
    }

    private void setListeners(int position) {
        layout.itemView.setOnClickListener(new OnOneClickListener() {
            @Override
            public void onOneClick(View v) {
                onItemClickedListener.onClicked(position);
            }
        });
    }
}
