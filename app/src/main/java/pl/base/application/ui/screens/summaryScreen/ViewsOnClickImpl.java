package pl.base.application.ui.screens.summaryScreen;

import android.view.View;

import lombok.AllArgsConstructor;
import pl.base.application.utils.OnOneClickListener;

@AllArgsConstructor
public class ViewsOnClickImpl extends OnOneClickListener {

    private SummaryActivity activity;

    @Override
    public void onOneClick(View view) {
        ViewHolder layout = activity.getLayout();
        int viewId = view.getId();
        if (viewId == layout.btnFinish.getId()) {
            activity.finish();
        }

    }
}
