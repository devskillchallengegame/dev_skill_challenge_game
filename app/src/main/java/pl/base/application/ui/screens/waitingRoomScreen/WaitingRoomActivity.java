package pl.base.application.ui.screens.waitingRoomScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import lombok.Getter;
import lombok.Setter;
import pl.base.application.cloud.rooms.RoomDestructor;
import pl.base.application.cloud.rooms.RoomUpdater;
import pl.base.application.models.Room;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.questionScreen.QuestionActivity;
import pl.base.application.ui.screens.waitingRoomScreen.memberAdapter.MemberAdapter;
import pl.base.application.utils.OnOneClickListener;
import pl.base.basic.R;
import pl.base.firebase_engine.auth.AuthManager;

import static android.support.v7.widget.RecyclerView.LayoutManager;
import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
import static pl.base.application.ui.screens.questionScreen.QuestionActivity.QUESTION_ACTIVITY_ROOM_EXTRAS_KEY;

public class WaitingRoomActivity extends BaseActivity {

    public static final String WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY = "waiting_room_activity_room_extras_key";
    @Setter
    @Getter
    private boolean gameStarts = false;
    @Getter
    private ViewHolder layout;
    @Getter
    private WaitingRoomActivityPresenter presenter;
    @Setter
    @Getter
    private Room room;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtras();
        initView();
        setPresenter();
        setListeners();
    }

    void prepareData() {
        MemberAdapter adapter = new MemberAdapter(room);
        String currentMembersCount = String.valueOf(room.getMembers().size());
        String maxMembersCount = String.valueOf(room.getMaxMembers());
        String roomName = String.valueOf(room.getName());
        String timeToAnswer = String.valueOf(room.getTimeToAnswer());
        String questionsQuantity = String.valueOf(room.getQuestionsCount());
        String difficultyLevel = "";
        switch (room.getDifficulty()) {
            case EASY:
                difficultyLevel = getString(R.string.difficulty_level_easy);
                break;
            case MEDIUM:
                difficultyLevel = getString(R.string.difficulty_level_medium);
                break;
            case HARD:
                difficultyLevel = getString(R.string.difficulty_level_hard);
                break;
        }
        layout.getTvQuestionsDifficulty().setText(difficultyLevel);
        layout.getTvCurrentMembersCount().setText(currentMembersCount);
        layout.getTvMaxMembersCount().setText(maxMembersCount);
        layout.getTvRoomName().setText(roomName);
        layout.getTvQuestionsCount().setText(questionsQuantity);
        layout.getTvQuestionsTime().setText(timeToAnswer);
        layout.getRecyclerView().setAdapter(adapter);

    }

    private void initView() {
        setContentView(R.layout.activity_waiting_room);
        View content = findViewById(android.R.id.content);
        getWindow().addFlags(FLAG_KEEP_SCREEN_ON);
        LayoutManager layoutManager = new LinearLayoutManager(this);
        layout = new ViewHolder(content);
        layout.getRecyclerView().setLayoutManager(layoutManager);
        layout.getRecyclerView().setHasFixedSize(true);
        prepareData();
        setListeners();
    }

    private void setPresenter() {
        presenter = new WaitingRoomActivityPresenter();
        presenter.onLoad(this);
        presenter.setButtonVisibility();
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            room = (Room) bundle.getSerializable(WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY);
        }
    }

    void showQuestionsScreen() {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(QUESTION_ACTIVITY_ROOM_EXTRAS_KEY, room);
        gameStarts = true;
        finish();
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        String userId = AuthManager.getInstance().getUserId();
        String roomId = room.getId();
        if (isUserGameOwner(room) && !gameStarts) {
            presenter.unregisterGetRoomListener();
            RoomDestructor roomDestructor = new RoomDestructor();
            roomDestructor.delete(roomId, null);
            finish();
        } else if (!gameStarts) {
            RoomUpdater roomUpdater = new RoomUpdater(roomId);
            roomUpdater.removeMember(userId, null);
        }
        super.onPause();
    }

    private void setListeners() {
        OnOneClickListener onClickListener = new ViewsOnClickImpl(this);
        layout.getBtnStart().setOnClickListener(onClickListener);
    }

    boolean isUserGameOwner(Room room) {
        String userId = AuthManager.getInstance().getUserId();
        return room.getOwnerId().equals(userId);
    }

}
