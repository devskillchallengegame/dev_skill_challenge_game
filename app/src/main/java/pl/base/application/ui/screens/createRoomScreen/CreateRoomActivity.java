package pl.base.application.ui.screens.createRoomScreen;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;

import lombok.Getter;
import pl.base.application.models.Question;
import pl.base.application.models.Room;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.questionScreen.QuestionActivity;
import pl.base.application.ui.screens.waitingRoomScreen.WaitingRoomActivity;
import pl.base.basic.R;

import static android.view.View.GONE;
import static pl.base.application.configs.PrefsConfig.LAST_ROOM_NAME_KEY;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.core.MyApplication.getPrefsCore;
import static pl.base.application.ui.screens.questionScreen.QuestionActivity.QUESTION_ACTIVITY_QUESTIONS_EXTRAS_KEY;
import static pl.base.application.ui.screens.questionScreen.QuestionActivity.QUESTION_ACTIVITY_ROOM_EXTRAS_KEY;
import static pl.base.application.ui.screens.waitingRoomScreen.WaitingRoomActivity.WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY;
import static pl.base.application.utils.DialogsUtils.showDefaultProgressDialog;
import static pl.base.application.utils.MessageBarsUtil.showToast;

public class CreateRoomActivity extends BaseActivity {

    @Getter
    private ViewHolder layout;
    @Getter
    private CreateRoomActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        prepareData();
        setPresenter();
    }

    private void initView() {
        setContentView(R.layout.activity_create_room);
        View content = findViewById(android.R.id.content);
        layout = new ViewHolder(content);
        if (!getGameConfig().isPvpMode()) {
            layout.etRoomName.setVisibility(GONE);
            layout.tvTimeToAnswer.setVisibility(GONE);
            layout.tvNumberOfPlayers.setVisibility(GONE);
            layout.tvDifficultyLevel.setVisibility(GONE);
            layout.tvNumberOfQuestions.setVisibility(GONE);
            layout.spinnerTimeToAnswer.setVisibility(GONE);
            layout.spinnerPlayersCount.setVisibility(GONE);
            layout.spinnerQuestionsCount.setVisibility(GONE);
            layout.spinnerDifficulty.setVisibility(GONE);
        }
        setListeners();
    }

    private void prepareData() {
        String lastRoomName = (String) getPrefsCore().getValue(LAST_ROOM_NAME_KEY, "");
        layout.etRoomName.setText(lastRoomName);
    }

    private void setListeners() {
        ViewsOnClickImpl onClickListener = new ViewsOnClickImpl(this);
        layout.btnCreate.setOnClickListener(onClickListener);
    }

    private void setPresenter() {
        presenter = new CreateRoomActivityPresenter();
        presenter.onLoad(this);
    }

    private boolean areFieldsValid() { // TODO: 24.01.2018 finish
        String roomName = layout.etRoomName.getText().toString();
        return !(TextUtils.isEmpty(roomName) || roomName.length() > 20);
    }

    void startQuestionsScreen(Room room) {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(QUESTION_ACTIVITY_ROOM_EXTRAS_KEY, room);
        finish();
        startActivity(intent);
    }

    void startQuestionsScreen(ArrayList<Question> questions) {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(QUESTION_ACTIVITY_QUESTIONS_EXTRAS_KEY, questions);
        finish();
        startActivity(intent);
    }

    void startWaitingRoomScreen(Room room) {
        Intent intent = new Intent(this, WaitingRoomActivity.class);
        intent.putExtra(WAITING_ROOM_ACTIVITY_ROOM_EXTRAS_KEY, room);
        finish();
        startActivity(intent);
    }

    void showCreateRoomErrorMsg() {
        showToast(this, R.string.create_room_activity_create_error);
    }

    void onCreateRoomClicked() { // TODO: 24.01.2018 refactor
        if (!getGameConfig().isPvpMode()) {
            getPresenter().getQuestionsFromSpreadsheet();
        } else if (areFieldsValid()) {
            showDefaultProgressDialog(this);
            String roomName = layout.etRoomName.getText().toString();
            getPrefsCore().putValue(LAST_ROOM_NAME_KEY, roomName);
            getPresenter().getQuestionsFromCloud();
        } else {
            layout.btnCreate.setActivated(false);
            showToast(this, R.string.create_room_activity_room_name_validation_error);
        }
    }

}