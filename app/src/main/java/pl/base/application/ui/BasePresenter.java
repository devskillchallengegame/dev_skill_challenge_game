package pl.base.application.ui;

public interface BasePresenter<T> {

    void onLoad(T activity);

}
