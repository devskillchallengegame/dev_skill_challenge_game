package pl.base.application.ui.screens.mainScreen;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import lombok.AllArgsConstructor;
import pl.base.basic.R;

@AllArgsConstructor
class ViewHolder {

    RelativeLayout containerLayout;
    Button btnStartGame;
    Button btnSettingsGame;
    Button btnRankingUser;

    ViewHolder(View view) {
        containerLayout = view.findViewById(R.id.activity_main_containter_layout);
        btnStartGame = view.findViewById(R.id.main_activity_start_game_btn);
        btnSettingsGame = view.findViewById(R.id.main_activity_settings_btn);
        btnRankingUser = view.findViewById(R.id.main_activity_ranking_btn);
    }

}
