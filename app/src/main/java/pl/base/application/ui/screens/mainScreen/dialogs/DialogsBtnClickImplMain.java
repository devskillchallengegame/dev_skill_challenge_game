package pl.base.application.ui.screens.mainScreen.dialogs;

import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import lombok.AllArgsConstructor;
import pl.base.application.ui.generalListeners.DialogCallbackListener;

@AllArgsConstructor
public class DialogsBtnClickImplMain implements MaterialDialog.SingleButtonCallback {

    private MainActivityDialogInfo dialogsInfo;
    private DialogCallbackListener listener;

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction action) {
        switch (dialogsInfo) {
            case START_GAME:
                listener.onCallback(dialog, action);
                break;
        }
    }
}
