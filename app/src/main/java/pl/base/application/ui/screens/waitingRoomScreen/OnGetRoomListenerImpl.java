package pl.base.application.ui.screens.waitingRoomScreen;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.Room;
import pl.base.basic.R;

import static pl.base.application.models.RoomStatus.DURING;
import static pl.base.application.utils.MessageBarsUtil.showToast;

@AllArgsConstructor
class OnGetRoomListenerImpl implements OnGetObjectListener<Room> {

    private WaitingRoomActivity activity;

    @Override
    public void onSuccess(Room room) {
        if (room != null) {
            activity.setRoom(room);
            if (!activity.isUserGameOwner(room) && room.getStatus().equals(DURING)) {
                activity.getPresenter().unregisterGetRoomListener();
                activity.showQuestionsScreen();
            } else {
                activity.prepareData();
            }
        } else {
            showToast(activity, R.string.waiting_room_activity_room_not_available_error_msg);
            activity.finish();
        }
    }

    @Override
    public void onFailure(ErrorStatus errorStatus) {

    }

    @Override
    public void onEnd() {

    }
}
