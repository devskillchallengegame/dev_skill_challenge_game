package pl.base.application.ui.screens.summaryScreen;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.RoomMember;
import pl.base.application.ui.screens.summaryScreen.memberAdapter.MemberAdapter;


@NoArgsConstructor
@AllArgsConstructor
public class OnGetMembersListenerImpl implements OnGetObjectListener<ArrayList<RoomMember>> {

    private SummaryActivity activity;

    @Override
    public void onSuccess(ArrayList<RoomMember> roomMembers) {
        ArrayList<RoomMember> filteredMembers = filterMembers(roomMembers);
        MemberAdapter memberAdapter = new MemberAdapter(filteredMembers);
        activity.getLayout().getRecyclerView().setAdapter(memberAdapter);
        memberAdapter.notifyDataSetChanged();
    }

    private ArrayList<RoomMember> filterMembers(ArrayList<RoomMember> roomMembers) {
        ArrayList<RoomMember> filteredMembers = new ArrayList<>();
        for (RoomMember member : roomMembers) {
            if (member.isHasFinished()) {
                filteredMembers.add(member);
            }
        }
        return filteredMembers;
    }

    @Override
    public void onFailure(ErrorStatus errorStatus) {
    }

    @Override
    public void onEnd() {
    }
}