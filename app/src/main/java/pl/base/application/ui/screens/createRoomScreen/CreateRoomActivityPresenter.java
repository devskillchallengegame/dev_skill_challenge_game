package pl.base.application.ui.screens.createRoomScreen;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.sheets.v4.SheetsScopes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import pl.base.application.cloud.questions.QuestionServant;
import pl.base.application.cloud.rooms.RoomCreator;
import pl.base.application.core.MyApplication;
import pl.base.application.models.DifficultyLevel;
import pl.base.application.models.Question;
import pl.base.application.models.QuestionCategory;
import pl.base.application.models.Room;
import pl.base.application.sheets.SpreadSheetRequest;
import pl.base.application.sheets.SpreadsheetDataCollector;
import pl.base.application.ui.BasePresenter;

import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.utils.DialogsUtils.hideProgressDialog;

public class CreateRoomActivityPresenter implements BasePresenter<CreateRoomActivity> {

    private CreateRoomActivity activity;
    private Room room;
    private int questionsCount;

    @Override
    public void onLoad(CreateRoomActivity activity) {
        this.activity = activity;
    }

    void createRoom(ArrayList<Question> questions) {
        String roomName = activity.getLayout().etRoomName.getText().toString();
        int numberOfPlayers = getNumberOfPlayers();
        int timeToAnswer = getTimeToAnswer();
        DifficultyLevel difficulty = getSelectedDifficultyLevel();
        if (MyApplication.getGameConfig().isPvpMode()) {
            RoomCreator roomCreator = new RoomCreator();
            room = roomCreator.create(roomName, numberOfPlayers, questionsCount, timeToAnswer, difficulty, questions);
            roomCreator.send(room, this::performActionOnRoomCreate);
        } else {
            activity.startQuestionsScreen(questions);
        }
    }

    private int getTimeToAnswer() {
        return Integer.parseInt(activity.getLayout().spinnerTimeToAnswer.getSelectedItem().toString());
    }

    private int getNumberOfPlayers() {
        return Integer.parseInt(activity.getLayout().spinnerPlayersCount.getSelectedItem().toString());
    }

    private DifficultyLevel getSelectedDifficultyLevel() {
        int difficultyPosition = activity.getLayout().spinnerDifficulty.getSelectedItemPosition();
        return DifficultyLevel.values()[difficultyPosition];
    }

    private void performActionOnRoomCreate(boolean isSuccess) {
        hideProgressDialog();
        if (isSuccess) {
            activity.startWaitingRoomScreen(room);
        } else {
            activity.showCreateRoomErrorMsg();
        }
    }

    void getQuestionsFromCloud() {
//        int difficultyPosition = activity.getLayout().spinnerDifficulty.getSelectedItemPosition();
//        DifficultyLevel difficulty = DifficultyLevel.values()[difficultyPosition];
        questionsCount = Integer.parseInt(activity.getLayout().spinnerQuestionsCount.getSelectedItem().toString());
        QuestionCategory category = getSelectedCategory();
        QuestionServant questionServant = new QuestionServant();
        questionServant.getQuestions(/*difficulty, */category, /*subcategory,*/ new OnGetQuestionsListenerImpl(activity));
    }

    private QuestionCategory getSelectedCategory() {
        int categoryPosition = activity.getLayout().spinnerCategory.getSelectedItemPosition();
        return QuestionCategory.values()[categoryPosition];
    }

    void getQuestionsFromSpreadsheet() {
        final String[] SCOPES = {SheetsScopes.SPREADSHEETS};
        GoogleCredential credentials = new GoogleCredential();
        credentials.createScoped(Arrays.asList(SCOPES));
        SpreadsheetDataCollector dataCollector = new SpreadsheetDataCollector(credentials, buildSpreadSheetRequest(), new OnGetQuestionsListenerImpl(activity));
        dataCollector.execute();
    }

    private SpreadSheetRequest buildSpreadSheetRequest() {
        return SpreadSheetRequest.builder()
                .apiKey(getGameConfig().getSpreadSheetApiKey())
                .spreadsheetId(getGameConfig().getSpreadSheetId())
                .sheetName(getSelectedCategory().toString().toUpperCase())
                .fromCell("A2")
                .toCell("B")
                .columnsSeparator("/#")
                .build();
    }

    ArrayList<Question> randomizeQuestions(ArrayList<Question> questions) {
        ArrayList<Question> randomQuestions = new ArrayList<>();
        int randomArray[] = generateRandUniqueSet(questionsCount, questions.size() - 1);
        for (int i : randomArray) {
            randomQuestions.add(questions.get(i));
        }
        return randomQuestions;
    }

    private int[] generateRandUniqueSet(int size, int bound) {
        int[] arrayResult = new int[size];
        Set<Integer> unique = new HashSet<>();
        Random rand = new Random();
        int counter = 0;
        while (counter < size) {
            int randValue = rand.nextInt(bound);
            if (unique.add(randValue)) {
                arrayResult[counter++] = randValue;
            }
        }
        return arrayResult;
    }

}