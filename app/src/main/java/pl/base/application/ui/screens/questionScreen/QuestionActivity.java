package pl.base.application.ui.screens.questionScreen;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;

import pl.base.application.cloud.rooms.RoomUpdater;
import pl.base.application.models.Answer;
import pl.base.application.models.Question;
import pl.base.application.models.Room;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.summaryScreen.SummaryActivity;
import pl.base.application.utils.OnOneClickListener;
import pl.base.application.utils.TouchableButton;
import pl.base.basic.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.ui.screens.summaryScreen.SummaryActivity.SUMMARY_ACTIVITY_ROOM_EXTRAS_KEY;

public class QuestionActivity extends BaseActivity {

    public static final String QUESTION_ACTIVITY_ROOM_EXTRAS_KEY = "question_activity_room_extras_key";
    public static final String QUESTION_ACTIVITY_QUESTIONS_EXTRAS_KEY = "question_activity_questions_extras_key";

    private ViewHolder layout;
    private QuestionActivityPresenter presenter;
    private CountDownTimer countDownTimer;
    private ArrayList<Question> questionsList;
    private int timeToAnswer;
    private int questionsCount;
    private int questionIndex = 0;
    private int points = 0;
    private Room room;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        prepareView();
        getExtras();
        setPresenter();
        setListeners();
        showNewQuestion();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
    }

    private void initView() {
        setContentView(R.layout.activity_question);
        View content = findViewById(android.R.id.content);
        getWindow().addFlags(FLAG_KEEP_SCREEN_ON);
        layout = new ViewHolder(content);
        layout.tvQuestionText.setMovementMethod(new ScrollingMovementMethod());
    }

    private void prepareView() {
        layout.tBtnAnswer1.setVisibility(getGameConfig().isAnswerFlashCard() ? VISIBLE : GONE);
        layout.tBtnAnswer2.setVisibility(getGameConfig().isAnswerFlashCard() ? GONE : VISIBLE);
        layout.tBtnAnswer3.setVisibility(getGameConfig().isAnswerFlashCard() ? GONE : VISIBLE);
        layout.tBtnAnswer4.setVisibility(getGameConfig().isAnswerFlashCard() ? GONE : VISIBLE);
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            room = (Room) bundle.getSerializable(QUESTION_ACTIVITY_ROOM_EXTRAS_KEY);
            Serializable questions = bundle.getSerializable(QUESTION_ACTIVITY_QUESTIONS_EXTRAS_KEY);
            if (room != null) {
                questionsList = room.getQuestions();
                timeToAnswer = room.getTimeToAnswer();
                questionsCount = questionsList.size();
            }
            if (questions != null) {
                questionsList = (ArrayList<Question>) questions;
                questionsCount = questionsList.size();
            }
        }
    }

    private void setPresenter() {
        presenter = new QuestionActivityPresenter();
        presenter.onLoad(this);
    }

    private void setListeners() {
        layout.ivArrowNextQuestion.setOnClickListener(new OnOneClickListener() {
            @Override
            public void onOneClick(View v) {
                stopTimer();
                checkAnswers();
                showNewQuestion();
            }
        });
    }

    private void showNewQuestion() {
        if (questionIndex < questionsCount) {
            Question question = questionsList.get(questionIndex);
            ArrayList<Answer> answers = question.getAnswers();
            layout.tvQuestionNumber.setText(String.valueOf(questionIndex + 1));
            layout.tvQuestionsCount.setText(String.valueOf(questionsCount));
            layout.tvQuestionText.setText(question.getQuestionText());

            if (getGameConfig().isAnswerFlashCard()) {
                layout.tBtnAnswer1.setUnchecked().setText(answers.get(0).getText());
                layout.tBtnAnswer1.setUnchecked().setText("Pokaż odpowiedź");
                layout.tBtnAnswer1.setOnClickListener(view -> {
                    view.setClickable(false);
                    ((TouchableButton) view).setText(answers.get(0).getText());
                });
            } else {
                layout.tBtnAnswer1.setUnchecked().setText(answers.get(0).getText());
                layout.tBtnAnswer2.setUnchecked().setText(answers.get(1).getText());
                layout.tBtnAnswer3.setUnchecked().setText(answers.get(2).getText());
                layout.tBtnAnswer4.setUnchecked().setText(answers.get(3).getText());
            }

            questionIndex++;
            if (getGameConfig().isPvpMode()) {
                startCounter();
            }

        } else if (getGameConfig().isPvpMode()) {
            showSummaryScreen();
        } else {
            finish();
        }
    }

    private void checkAnswers() {
        ArrayList<Answer> answers = questionsList.get(questionIndex - 1).getAnswers();
        ArrayList<Boolean> givenAnswersCorrection = new ArrayList<>();
        ArrayList<Boolean> answersCorrection = new ArrayList<>();
        givenAnswersCorrection.add(layout.tBtnAnswer1.isChecked());
        givenAnswersCorrection.add(layout.tBtnAnswer2.isChecked());
        givenAnswersCorrection.add(layout.tBtnAnswer3.isChecked());
        givenAnswersCorrection.add(layout.tBtnAnswer4.isChecked());
        for (Answer answer : answers) {
            answersCorrection.add(answer.isCorrect());
        }
        if (givenAnswersCorrection.equals(answersCorrection)) {
            points += 1;
        }
    }

    private void showSummaryScreen() {
        Intent intent = new Intent(this, SummaryActivity.class);

        intent.putExtra(SUMMARY_ACTIVITY_ROOM_EXTRAS_KEY, room);
        intent.putExtra("POINTS", points);

        RoomUpdater roomUpdater = new RoomUpdater(room.getId());
        roomUpdater.updateMemberParameters(points, true, isSuccess -> {
            if (isSuccess) {
                finish();
                startActivity(intent);
            }
        });
    }

    private void startCounter() {
        countDownTimer = new CountDownTimer(timeToAnswer * 1000, 100) {
            @Override
            public void onTick(long timeRemaining) {
                int timeInPercentage = (int) ((timeRemaining / 10) / timeToAnswer);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    layout.pbTimeCollapsing.setProgress(timeInPercentage, true);
                } else {
                    layout.pbTimeCollapsing.setProgress(timeInPercentage);
                }
            }

            @Override
            public void onFinish() {
                checkAnswers();
                showNewQuestion();
            }
        }.start();
    }

    private void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}