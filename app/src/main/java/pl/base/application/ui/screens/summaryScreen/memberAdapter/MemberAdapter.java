package pl.base.application.ui.screens.summaryScreen.memberAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import pl.base.application.models.RoomMember;
import pl.base.basic.R;

@AllArgsConstructor
public class MemberAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<RoomMember> roomMembers;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_member_summary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RoomMember roomMember = roomMembers.get(position);
        String userName = roomMember.getUserName();
        String points = String.valueOf(roomMember.getPoints());
        holder.getTvMemberName().setText(userName);
        holder.getTvMemberPoints().setText(points);
    }

    @Override
    public int getItemCount() {
        return roomMembers.size();
    }
}
