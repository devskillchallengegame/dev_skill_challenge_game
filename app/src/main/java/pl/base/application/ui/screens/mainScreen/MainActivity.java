package pl.base.application.ui.screens.mainScreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import lombok.Getter;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.dialogs.DialogsManager;
import pl.base.application.ui.screens.createRoomScreen.CreateRoomActivity;
import pl.base.application.ui.screens.rankingScreen.RankingListActivity;
import pl.base.application.ui.screens.roomsListScreen.RoomsListActivity;
import pl.base.application.ui.screens.settingsScreen.SettingsActivity;
import pl.base.application.utils.SoundsUtils;
import pl.base.basic.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.utils.MessageBarsUtil.showToast;

public class MainActivity extends BaseActivity {

    @Getter
    private MainActivityPresenter presenter;
    @Getter
    private ViewHolder layout;
    private DialogsManager dialogsManager;
    private boolean isBackPressedLocked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        prepareView();
        setPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (isBackPressedLocked) {
            isBackPressedLocked = false;
            showToast(this, R.string.main_activity_on_back_pressed_message);
            new Handler().postDelayed(() -> isBackPressedLocked = true, 2000);
        } else {
            SoundsUtils.stopMusic();
            finishAffinity();
        }
    }

    private void initView() {
        setContentView(R.layout.activity_main);
        View content = findViewById(android.R.id.content);
        layout = new ViewHolder(content);
        dialogsManager = new DialogsManager();
        setListeners();
    }

    private void prepareView() {
        layout.btnRankingUser.setVisibility(getGameConfig().isPvpMode() ? VISIBLE : GONE);
    }

    private void setPresenter() {
        presenter = new MainActivityPresenter();
        presenter.onLoad(this);
    }

    private void setListeners() {
        ViewsOnClickImpl onClickListener = new ViewsOnClickImpl(this);
        layout.btnStartGame.setOnClickListener(onClickListener);
        layout.btnSettingsGame.setOnClickListener(onClickListener);
        layout.btnRankingUser.setOnClickListener(onClickListener);
    }

    void showStartGameDialog() {
        dialogsManager.showStartGameDialog(this, (dialog, action) -> {
            switch (action) {
                case POSITIVE:
                    startRoomsListScreen();
                    break;
                case NEUTRAL:
                    dialog.dismiss();
                    break;
                case NEGATIVE:
                    startCreateRoomScreen();
                    break;
            }
        });
    }

    void showEnterNickDialog() {
        dialogsManager.showNickChangeDialog(this, false, (dialog, action) -> {
            switch (action) {
                case POSITIVE:
                    String userName = dialog.getInputEditText().getText().toString();
                    presenter.addNewUserRecord(userName);
                    break;
            }
        });
    }

    void startRoomsListScreen() {
        Intent intent = new Intent(this, RoomsListActivity.class);
        startActivity(intent);
    }

    void startCreateRoomScreen() {
        Intent intent = new Intent(this, CreateRoomActivity.class);
        startActivity(intent);
    }

    void startSettingsScreen() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    void startRankingScreen() {
        Intent intent = new Intent(this, RankingListActivity.class);
        startActivity(intent);
    }
}
