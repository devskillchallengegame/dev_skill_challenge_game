package pl.base.application.ui.screens.mainScreen;

import pl.base.application.cloud.users.UserCreator;
import pl.base.application.ui.BasePresenter;
import pl.base.firebase_engine.auth.AuthManager;

import static pl.base.application.utils.DialogsUtils.hideProgressDialog;
import static pl.base.application.utils.DialogsUtils.showDefaultProgressDialog;

class MainActivityPresenter implements BasePresenter<MainActivity> {

    private MainActivity activity;
    private AuthManager authManager;

    @Override
    public void onLoad(MainActivity activity) {
        this.activity = activity;
        authManager = AuthManager.getInstance();
        autoLogin();
    }

    private void autoLogin() {
        if (!authManager.isSigned()) {
            signIn();
        }
    }

    void addNewUserRecord(String userName) {
        String userId = authManager.getUserId();
        UserCreator userCreator = new UserCreator();
        userCreator.create(userId, userName);
    }

    private void signIn() {
        showDefaultProgressDialog(activity);
        AuthManager authManager = AuthManager.getInstance();
        authManager.signInAnonymously(isSuccess -> {
            hideProgressDialog();
            if (isSuccess) {
                activity.showEnterNickDialog();
            } else {
                // TODO: 01.11.2017
            }
        });
    }
}
