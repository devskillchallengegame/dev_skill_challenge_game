package pl.base.application.ui.screens.splashScreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.trncic.library.DottedProgressBar;

import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.screens.mainScreen.MainActivity;
import pl.base.basic.R;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SplashActivity extends BaseActivity {

    private static boolean IS_SPLASH_ON_TOP = true;
    private DottedProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        IS_SPLASH_ON_TOP = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar = findViewById(R.id.progress);
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        IS_SPLASH_ON_TOP = false;
    }

    public static boolean isActivityOnTop() {
        return IS_SPLASH_ON_TOP;
    }

    private void startTimer() {
        CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long l) {
                //TODO load progress bar...
            }

            @Override
            public void onFinish() {
                progressBar.stopProgress();
                startMainActivity();
            }
        };
        countDownTimer.start();
        progressBar.startProgress();

    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
        startMainActivity(intent);
    }

}