package pl.base.application.ui.screens.createRoomScreen;

import android.view.View;

import pl.base.application.utils.OnOneClickListener;

class ViewsOnClickImpl extends OnOneClickListener {

    private final CreateRoomActivity activity;

    ViewsOnClickImpl(CreateRoomActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onOneClick(View view) {
        int clickedViewId = view.getId();
        ViewHolder layout = activity.getLayout();
        if (clickedViewId == layout.btnCreate.getId()) {
            activity.onCreateRoomClicked();
        }
    }

}
