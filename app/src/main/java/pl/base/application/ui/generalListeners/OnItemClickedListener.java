package pl.base.application.ui.generalListeners;

public interface OnItemClickedListener {

    void onClicked(int position);

}