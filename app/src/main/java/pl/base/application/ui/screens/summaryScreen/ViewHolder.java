package pl.base.application.ui.screens.summaryScreen;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import lombok.Getter;
import pl.base.basic.R;

@Getter
class ViewHolder {

    RecyclerView recyclerView;
    Button btnFinish;
    ProgressBar progressBar;

    ViewHolder(View view) {
        recyclerView = view.findViewById(R.id.summary_room_activity_rv);
        btnFinish = view.findViewById(R.id.summary_room_activity_finish_btn);
    }
}