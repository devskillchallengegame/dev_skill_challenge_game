package pl.base.application.ui.screens.mainScreen;

import android.view.View;

import lombok.AllArgsConstructor;
import pl.base.application.core.MyApplication;
import pl.base.application.utils.ConnectivityUtils;
import pl.base.application.utils.MessageBarsUtil;
import pl.base.application.utils.OnOneClickListener;

@AllArgsConstructor
class ViewsOnClickImpl extends OnOneClickListener {

    private MainActivity activity;

    @Override
    public void onOneClick(View view) {
        if (ConnectivityUtils.isNetworkAvailable(activity)) {
            ViewHolder layout = activity.getLayout();
            if (view.getId() == layout.btnStartGame.getId()) {
                if (MyApplication.getGameConfig().isPvpMode()) {
                    activity.showStartGameDialog();
                } else {
                    activity.startCreateRoomScreen();
                }
            } else if (view.getId() == layout.btnSettingsGame.getId()) {
                activity.startSettingsScreen();
            } else if (view.getId() == layout.btnRankingUser.getId()) {
                activity.startRankingScreen();
            }
        } else {
            MessageBarsUtil.showToast(activity, "Włącz internet!");
        }
    }

}
