package pl.base.application.ui.screens.rankingScreen.rankingAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pl.base.basic.R;

class ViewHolder extends RecyclerView.ViewHolder {

    TextView tvUserName, tvPosition, tvPoints;
    ImageView ivLeader;

    ViewHolder(View itemView) {
        super(itemView);
        tvUserName = itemView.findViewById(R.id.item_ranking_user_tv_user_name);
        tvPosition = itemView.findViewById(R.id.item_ranking_user_tv_position);
        tvPoints = itemView.findViewById(R.id.item_ranking_user_tv_points);
        ivLeader = itemView.findViewById(R.id.item_ranking_user_iv_leader);
    }

}
