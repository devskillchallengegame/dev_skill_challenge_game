package pl.base.application.ui.screens.rankingScreen;

import java.util.ArrayList;

import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.cloud.users.UserServant;
import pl.base.application.models.User;
import pl.base.application.ui.BasePresenter;
import pl.base.application.ui.generalListeners.OnItemClickedListener;

public class RankingListActivityPresenter implements BasePresenter<RankingListActivity> {

    private RankingListActivity activity;
    private OnItemClickedListener onItemClickedListener;
    private OnGetObjectListener<ArrayList<User>> getUserListener;
    private ArrayList<User> users;

    @Override
    public void onLoad(RankingListActivity activity) {
        this.activity = activity;
        setListeners();
        sendGetUsersRequest();
    }

    void sendGetUsersRequest() {
        UserServant userServant = new UserServant(true);
        userServant.getUsers(getUserListener);
    }

    void setLocalUsersList(ArrayList<User> users) {
        this.users = users;
    }

    private void setListeners() {
        setOnItemClickedListener();
        setOnGetUsersListener();
    }

    private void setOnItemClickedListener() {
        onItemClickedListener = position -> {
            User user = users.get(position);
            //przeniesienie do profilu gracza ???
        };
    }

    private void setOnGetUsersListener() {
        getUserListener = new OnGetUsersListenerImpl(activity, onItemClickedListener);
    }
}
