package pl.base.application.ui.screens.roomsListScreen.roomAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pl.base.basic.R;

class ViewHolder extends RecyclerView.ViewHolder {

    TextView tvRoomName;
    TextView tvMembersCount;
    TextView tvMembersMax;

    ViewHolder(View itemView) {
        super(itemView);
        tvRoomName = itemView.findViewById(R.id.item_room_tv_room_name);
        tvMembersCount = itemView.findViewById(R.id.item_room_tv_members_count);
        tvMembersMax = itemView.findViewById(R.id.item_room_tv_max_members_count);
    }

}
