package pl.base.application.ui.screens.settingsScreen;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import lombok.AllArgsConstructor;
import pl.base.basic.R;


@AllArgsConstructor
class ViewHolder {

    LinearLayout containerLayout;
    CardView importQuestionsContainerLayout;
    TextView tvNick, tvQuestionsImportStatus;
    ProgressBar progressBar;
    Button btnChange, btnImportQuestions, btnSendQuestions;
    CheckBox cbSound;
    CardView nickFieldContainer;

    ViewHolder(View view) {
        containerLayout = view.findViewById(R.id.activity_settings_container_layout);
        importQuestionsContainerLayout = view.findViewById(R.id.activity_settings_cv_import_questions);
        tvNick = view.findViewById(R.id.activity_settings_tv_nick);
        tvQuestionsImportStatus = view.findViewById(R.id.activity_settings_tv_questions_import_status);
        progressBar = view.findViewById(R.id.activity_settings_pb);
        btnChange = view.findViewById(R.id.activity_settings_change_btn);
        btnImportQuestions = view.findViewById(R.id.activity_settings_btn_import_questions);
        btnSendQuestions = view.findViewById(R.id.activity_settings_btn_send_questions);
        cbSound = view.findViewById(R.id.activity_settings_cb_sound);
        nickFieldContainer = view.findViewById(R.id.activity_settings_cardView_nick);
    }


}
