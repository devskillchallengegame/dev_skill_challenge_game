package pl.base.application.ui.screens.roomsListScreen;

import java.util.ArrayList;

import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.cloud.rooms.RoomServant;
import pl.base.application.cloud.rooms.RoomUpdater;
import pl.base.application.models.Room;
import pl.base.application.ui.BasePresenter;
import pl.base.application.ui.generalListeners.OnItemClickedListener;
import pl.base.firebase_engine.auth.AuthManager;

class RoomsListActivityPresenter implements BasePresenter<RoomsListActivity> {

    private RoomsListActivity activity;
    private OnItemClickedListener onItemClickedListener;
    private ArrayList<Room> rooms;
    private RoomServant roomServant;
    private OnGetObjectListener<ArrayList<Room>> onGetRoomsListener;

    @Override
    public void onLoad(RoomsListActivity activity) {
        this.activity = activity;
        setListeners();
    }

    void getRooms() {
        activity.showProgressBar();
        roomServant = new RoomServant();
        roomServant.getRooms(onGetRoomsListener);
    }

    void unregisterGetRoomsListener() {
        if (roomServant != null) {
            roomServant.unregisterGetRoomsListener();
        }
    }

    void setLocalRoomsList(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    private void setListeners() {
        onItemClickedListener = this::joinToRoom;
        onGetRoomsListener = new OnGetRoomsListenerImpl(activity, onItemClickedListener);
    }

    private void joinToRoom(int position) {
        Room room = rooms.get(position);
        RoomUpdater roomUpdater = new RoomUpdater(room.getId());
        String userId = AuthManager.getInstance().getUserId();
        String userName = AuthManager.getInstance().getUserName();
        unregisterGetRoomsListener();
        roomUpdater.addMember(userId, userName, isSuccess -> {
            if (isSuccess) {
                activity.startWaitingRoomScreen(room);
            }
            //else TODO handle this case;
        });
    }
}