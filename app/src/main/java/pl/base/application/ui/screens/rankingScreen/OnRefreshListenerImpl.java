package pl.base.application.ui.screens.rankingScreen;

import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class OnRefreshListenerImpl implements OnRefreshListener {

    private RankingListActivity activity;

    @Override
    public void onRefresh() {
        activity.getPresenter().sendGetUsersRequest();
    }
}
