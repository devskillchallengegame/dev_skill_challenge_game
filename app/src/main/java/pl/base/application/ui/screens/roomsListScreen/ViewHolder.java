package pl.base.application.ui.screens.roomsListScreen;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import pl.base.basic.R;

class ViewHolder {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView tvEmptyRoomsList;

    ViewHolder(View view) {
        swipeRefreshLayout = view.findViewById(R.id.rooms_list_activity_srl);
        recyclerView = view.findViewById(R.id.rooms_list_activity_rv);
        progressBar = view.findViewById(R.id.rooms_list_activity_pb);
        tvEmptyRoomsList = view.findViewById(R.id.rooms_activity_empty_rooms_list_tv);
    }

}
