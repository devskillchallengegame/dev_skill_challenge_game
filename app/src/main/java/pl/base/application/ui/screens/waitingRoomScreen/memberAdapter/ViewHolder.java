package pl.base.application.ui.screens.waitingRoomScreen.memberAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lombok.Getter;
import pl.base.basic.R;

@Getter
class ViewHolder extends RecyclerView.ViewHolder {

    private TextView tvMemberName;
    ImageView imageRoomOwner;

    ViewHolder(View itemView) {
        super(itemView);
        tvMemberName = itemView.findViewById(R.id.item_member_tv_nick);
        imageRoomOwner = itemView.findViewById(R.id.item_member_image_view);
    }

}
