package pl.base.application.ui.screens.summaryScreen;

import pl.base.application.cloud.rooms.RoomServant;
import pl.base.application.models.Room;
import pl.base.application.ui.BasePresenter;

public class SummaryActivityPresenter implements BasePresenter<SummaryActivity> {

    private SummaryActivity activity;

    @Override
    public void onLoad(SummaryActivity activity) {
        this.activity = activity;
        getMembers();
    }

    private void getMembers() {
        Room room = activity.getRoom();
        String roomId = room.getId();
        RoomServant roomServant = new RoomServant();
        roomServant.getMembers(roomId, new OnGetMembersListenerImpl(activity));
    }
}
