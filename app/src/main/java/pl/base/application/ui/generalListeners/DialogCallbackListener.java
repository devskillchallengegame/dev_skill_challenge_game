package pl.base.application.ui.generalListeners;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public interface DialogCallbackListener {

    void onCallback(MaterialDialog dialog, DialogAction action);

}
