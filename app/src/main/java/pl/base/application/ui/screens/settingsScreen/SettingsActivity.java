package pl.base.application.ui.screens.settingsScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nononsenseapps.filepicker.FilePickerActivity;

import lombok.Getter;
import pl.base.application.ui.BaseActivity;
import pl.base.application.ui.dialogs.DialogsManager;
import pl.base.basic.R;
import pl.base.firebase_engine.auth.AuthManager;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.os.Environment.getExternalStorageDirectory;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_CREATE_DIR;
import static com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_MULTIPLE;
import static com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_MODE;
import static com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_START_PATH;
import static com.nononsenseapps.filepicker.FilePickerActivity.MODE_FILE;
import static pl.base.application.configs.PrefsConfig.SOUND_PREFS_KEY;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.core.MyApplication.getPrefsCore;

public class SettingsActivity extends BaseActivity {

    @Getter
    private SettingsActivityPresenter presenter;
    private static final int READ_FILES_PERMISSION_REQUEST_CODE = 31238;
    private static final int FILE_PICK_ACTIVITY_REQUEST_CODE = 23948;

    @Getter
    private ViewHolder layout;
    private DialogsManager dialogsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        prepareView();
        setListeners();
        setPresenter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == FILE_PICK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            presenter.handleFilePick(intent);
            layout.tvQuestionsImportStatus.setText("Imported");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_FILES_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    showFilesManagerActivity();
                }
            }
        }
    }

    void showNickChangeDialog() {
        dialogsManager.showNickChangeDialog(this, true, (dialog, action) -> {
            switch (action) {
                case NEGATIVE:
                    dialog.dismiss();
                    break;
                case POSITIVE:
                    startProgressBar();
                    String nick = getTextFromDialog(dialog);
                    presenter.changeNick(nick);
                    break;
            }
        });
    }

    private void startProgressBar() {
        layout.progressBar.setVisibility(VISIBLE);
        layout.tvNick.setVisibility(INVISIBLE);
    }

    void askForReadFilesPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE},
                READ_FILES_PERMISSION_REQUEST_CODE);
    }

    void setDisplayingUserName() {
        String userName = AuthManager.getInstance().getUserName();
        layout.tvNick.setText(userName);
        layout.tvNick.setVisibility(VISIBLE);
    }

    private void showFilesManagerActivity() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(EXTRA_ALLOW_MULTIPLE, false);
        intent.putExtra(EXTRA_ALLOW_CREATE_DIR, false);
        intent.putExtra(EXTRA_MODE, MODE_FILE);
        intent.putExtra(EXTRA_START_PATH, getExternalStorageDirectory().getPath());
        startActivityForResult(intent, FILE_PICK_ACTIVITY_REQUEST_CODE);
    }

    private void setPresenter() {
        presenter = new SettingsActivityPresenter();
        presenter.onLoad(this);
    }

    private void initView() {
        setContentView(R.layout.activity_settings);
        View content = findViewById(android.R.id.content);
        layout = new ViewHolder(content);
        dialogsManager = new DialogsManager();
    }

    private void prepareView() {
        setDisplayingUserName();
        layout.importQuestionsContainerLayout.setVisibility(getGameConfig().isAdmin() ? VISIBLE : GONE);
        layout.nickFieldContainer.setVisibility(getGameConfig().isPvpMode() ? VISIBLE : GONE);
        layout.cbSound.setChecked((Boolean) getPrefsCore().getValue(SOUND_PREFS_KEY, getGameConfig().playMusicOnDefault()));
    }

    private void setListeners() {
        ViewsOnClickImpl onClickListener = new ViewsOnClickImpl(this);
        layout.cbSound.setOnClickListener(onClickListener);
        layout.btnChange.setOnClickListener(onClickListener);
        layout.btnImportQuestions.setOnClickListener(onClickListener);
        layout.btnSendQuestions.setOnClickListener(onClickListener);
    }

    private String getTextFromDialog(MaterialDialog dialog) {
        EditText dialogEditText = dialog.getInputEditText();
        String text = AuthManager.getInstance().getUserName();
        if (dialogEditText != null) {
            text = dialogEditText.getText().toString();
        }
        return text;
    }

}