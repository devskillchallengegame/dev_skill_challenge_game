package pl.base.application.ui.screens.summaryScreen.memberAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import lombok.Getter;
import pl.base.basic.R;

@Getter
class ViewHolder extends RecyclerView.ViewHolder {

    private TextView tvMemberName;
    private TextView tvMemberPoints;

    ViewHolder(View itemView) {
        super(itemView);
        tvMemberName = itemView.findViewById(R.id.item_member_summary_tv_nick);
        tvMemberPoints = itemView.findViewById(R.id.item_member_summary_tv_points);
    }

}
