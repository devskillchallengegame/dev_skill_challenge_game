package pl.base.application.ui.dialogs;

import android.content.Context;
import android.text.InputType;

import com.afollestad.materialdialogs.MaterialDialog;

import pl.base.application.ui.generalListeners.DialogCallbackListener;
import pl.base.application.ui.screens.mainScreen.dialogs.DialogsBtnClickImplMain;
import pl.base.application.ui.screens.settingsScreen.dialogs.DialogsBtnClickImplSettings;
import pl.base.basic.R;

import static pl.base.application.ui.screens.mainScreen.dialogs.MainActivityDialogInfo.START_GAME;
import static pl.base.application.ui.screens.settingsScreen.dialogs.SettingsActivityDialogInfo.CHANGE_NICK;

public class DialogsManager {

    public MaterialDialog showStartGameDialog(Context context, DialogCallbackListener listener) {
        DialogsBtnClickImplMain singleButtonCallback = new DialogsBtnClickImplMain(START_GAME, listener);
        return new MaterialDialog.Builder(context)
                .title(R.string.main_activity_start_game_dialog_title)
                .content(R.string.main_activity_start_game_dialog_content)
                .positiveText(R.string.main_activity_start_game_dialog_join_room_btn)
                .neutralText(R.string.main_activity_start_game_dialog_cancel_btn)
                .negativeText(R.string.main_activity_start_game_dialog_new_room_btn)
                .onAny(singleButtonCallback)
                .show();
    }

    public MaterialDialog showNickChangeDialog(Context context, boolean cancelable, DialogCallbackListener listener) {
        int positiveId = R.string.general_ok_capital;
        int negativeId = cancelable ? R.string.general_cancel_capital : 0;
        DialogsBtnClickImplSettings singleButtonCallback = new DialogsBtnClickImplSettings(CHANGE_NICK, listener);
        return new MaterialDialog.Builder(context)
                .title(R.string.settings_activity_nick_change_dialog_title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(R.string.default_username, 0, (dialog, input) -> {
                })
                .inputRange(4, 30)
                .positiveText(positiveId)
                .negativeText(negativeId)
                .onAny(singleButtonCallback)
                .cancelable(cancelable)
                .show();
    }

}