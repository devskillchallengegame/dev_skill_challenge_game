package pl.base.application.ui.screens.rankingScreen;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import lombok.Getter;
import pl.base.basic.R;

@Getter
class ViewHolder {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView tvEmptyUsersList;

    ViewHolder(View view) {
        swipeRefreshLayout = view.findViewById(R.id.ranking_activity_srl);
        recyclerView = view.findViewById(R.id.ranking_activity_rv);
        progressBar = view.findViewById(R.id.ranking_activity_pb);
        tvEmptyUsersList = view.findViewById(R.id.ranking_activity_empty_user_list_tv);
    }


}
