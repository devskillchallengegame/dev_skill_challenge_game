package pl.base.application.ui.screens.createRoomScreen;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import pl.base.basic.R;

public class ViewHolder {

    EditText etRoomName;
    Spinner spinnerPlayersCount;
    Spinner spinnerTimeToAnswer;
    Spinner spinnerDifficulty, spinnerCategory;
    Spinner spinnerQuestionsCount;
    Button btnCreate;
    TextView tvNumberOfPlayers, tvTimeToAnswer, tvNumberOfQuestions, tvDifficultyLevel;

    public ViewHolder(View view) {
        etRoomName = view.findViewById(R.id.activity_create_room_vs_room_name);
        spinnerPlayersCount = view.findViewById(R.id.activity_create_room_dropdown_nr_of_players);
        spinnerTimeToAnswer = view.findViewById(R.id.activity_create_room_dropdown_time_to_answer);
        spinnerDifficulty = view.findViewById(R.id.activity_create_room_spinner_difficulty_level);
        spinnerCategory = view.findViewById(R.id.activity_create_room_spinner_category);
        spinnerQuestionsCount = view.findViewById(R.id.activity_create_room_dropdown_nr_of_questions);
        btnCreate = view.findViewById(R.id.activity_create_room_create_button);
        tvNumberOfPlayers = view.findViewById(R.id.activity_create_room_vs_nr_of_players_string);
        tvTimeToAnswer = view.findViewById(R.id.activity_create_room_vs_time_to_answer_string);
        tvNumberOfQuestions = view.findViewById(R.id.activity_create_room_vs_numb_of_quest_text);
        tvDifficultyLevel = view.findViewById(R.id.activity_create_room_tv_difficulty_level);
    }
}