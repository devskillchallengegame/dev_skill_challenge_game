package pl.base.application.ui.screens.waitingRoomScreen;

import android.view.View;

import pl.base.application.utils.OnOneClickListener;

class ViewsOnClickImpl extends OnOneClickListener {

    private final WaitingRoomActivity activity;

    ViewsOnClickImpl(WaitingRoomActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onOneClick(View view) {
        int clickedViewId = view.getId();
        ViewHolder layout = activity.getLayout();
        if (clickedViewId == layout.btnStart.getId()) {
            activity.getPresenter().startGame();
        }
    }

}
