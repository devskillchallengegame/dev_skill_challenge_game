package pl.base.application.ui.screens.waitingRoomScreen;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import lombok.Getter;
import pl.base.basic.R;

@Getter
class ViewHolder {

    RecyclerView recyclerView;
    Button btnStart;
    TextView tvRoomName, tvCurrentMembersCount,
            tvMaxMembersCount, tvQuestionsDifficulty, tvQuestionsTime, tvQuestionsCount;
    ProgressBar progressBar;

    ViewHolder(View view) {
        recyclerView = view.findViewById(R.id.waiting_room_activity_rv);
        btnStart = view.findViewById(R.id.waiting_room_activity_btn_start);
        tvRoomName = view.findViewById(R.id.waiting_room_activity_tv_room_name);
        tvCurrentMembersCount = view.findViewById(R.id.waiting_room_activity_tv_members_count);
        tvMaxMembersCount = view.findViewById(R.id.waiting_room_activity_tv_max_members_count);
        tvQuestionsDifficulty = view.findViewById(R.id.waiting_room_activity_tv_level);
        tvQuestionsTime = view.findViewById(R.id.waiting_room_activity_tv_time_to_answer);
        tvQuestionsCount = view.findViewById(R.id.waiting_room_activity_tv_questions_count);
        progressBar = view.findViewById(R.id.waiting_room_activity_pb);
    }

}