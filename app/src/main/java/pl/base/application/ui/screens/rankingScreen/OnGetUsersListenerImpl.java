package pl.base.application.ui.screens.rankingScreen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.User;
import pl.base.application.ui.generalListeners.OnItemClickedListener;
import pl.base.application.ui.screens.rankingScreen.rankingAdapter.UserAdapter;

import static android.view.View.VISIBLE;

@AllArgsConstructor
class OnGetUsersListenerImpl implements OnGetObjectListener<ArrayList<User>> {

    private RankingListActivity activity;
    private OnItemClickedListener onItemClickedListener;


    @Override
    public void onSuccess(ArrayList<User> users) {
        if (users.isEmpty()) {
            activity.getLayout().tvEmptyUsersList.setVisibility(VISIBLE);
        } else {
            sortUserByScore(users);
            UserAdapter userAdapter = new UserAdapter(users, onItemClickedListener);
            activity.getPresenter().setLocalUsersList(users);
            activity.showUsers(userAdapter);
        }
    }

    @Override
    public void onFailure(ErrorStatus errorStatus) {
        activity.showGetRankingError();
    }

    @Override
    public void onEnd() {
        activity.getLayout().swipeRefreshLayout.setRefreshing(false);
        activity.hideProgressBar();
    }

    private void sortUserByScore(ArrayList<User> usersList) {
        Collections.sort(usersList, new Comparator<User>() {
            public int compare(User o1, User o2) {
                if (o1.getPoints() == o2.getPoints())
                    return 0;
                return o1.getPoints() > o2.getPoints() ? -1 : 1;
            }
        });
    }
}
