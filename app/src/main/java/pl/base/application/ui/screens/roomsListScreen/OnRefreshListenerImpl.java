package pl.base.application.ui.screens.roomsListScreen;

import android.support.v4.widget.SwipeRefreshLayout;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class OnRefreshListenerImpl implements SwipeRefreshLayout.OnRefreshListener {
    private RoomsListActivity activity;

    @Override
    public void onRefresh() {
        activity.getPresenter().getRooms();
    }
}