package pl.base.application.ui.screens.waitingRoomScreen.memberAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.base.application.models.Room;
import pl.base.application.models.RoomMember;
import pl.base.application.utils.CollectionsUtils;
import pl.base.basic.R;

public class MemberAdapter extends RecyclerView.Adapter<ViewHolder> {

    private Room room;

    public MemberAdapter(Room room) {
        this.room = room;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_member, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ArrayList<RoomMember> roomMembers = CollectionsUtils.getListFromSet(room.getMembers());
        RoomMember roomMember = roomMembers.get(position);
        String userName = roomMember.getUserName();
        holder.getTvMemberName().setText(userName);
        if (room.getOwnerId().equals(roomMember.getId())) {
            holder.getImageRoomOwner().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return room.getMembers().size();
    }
}
