package pl.base.application.cloud.rooms;

import com.google.firebase.database.DataSnapshot;

import java.text.ParseException;
import java.util.ArrayList;

import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.Room;
import pl.base.application.models.RoomMember;
import pl.base.firebase_engine.database.FailureStatus;
import pl.base.firebase_engine.database.GetQuery;
import pl.base.firebase_engine.database.interfaces.OnGetValueListener;

import static pl.base.application.cloud.ErrorStatus.CONNECTION_ERROR;
import static pl.base.application.cloud.ErrorStatus.PARSE_ERROR;
import static pl.base.application.configs.ConnectionConfig.ROOMS_REFERENCE;

public class RoomServant {

    private GetQuery getQuery;
    private OnGetObjectListener<Room> getRoomListener;
    private OnGetObjectListener<ArrayList<Room>> getRoomsListener;

    private OnGetObjectListener<ArrayList<RoomMember>> getRoomMembersListener;

    public RoomServant() {
        getQuery = new GetQuery(true);
    }

    public void unregisterGetRoomListener() {
        getRoomListener = null;
    }

    public void unregisterGetRoomsListener() {
        getRoomsListener = null;
    }

    public void getRoom(String roomId, OnGetObjectListener<Room> listener) {
        this.getRoomListener = listener;
        getQuery.getValue(ROOMS_REFERENCE + roomId, new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                if (getRoomListener != null) {
                    try {
                        Room room = parseRoom(dataSnapshot);
                        getRoomListener.onEnd();
                        getRoomListener.onSuccess(room);
                    } catch (ParseException e) {
                        getRoomListener.onEnd();
                        getRoomListener.onFailure(PARSE_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                if (listener != null) {
                    listener.onFailure(CONNECTION_ERROR);
                }
            }
        });
    }

    public void getRooms(OnGetObjectListener<ArrayList<Room>> listener) {
        this.getRoomsListener = listener;
        getQuery.getValue(ROOMS_REFERENCE, new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                if (getRoomsListener != null) {
                    try {
                        ArrayList<Room> rooms = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Room room = parseRoom(snapshot);
                            rooms.add(room);
                        }
                        getRoomsListener.onSuccess(rooms);
                    } catch (ParseException e) {
                        getRoomsListener.onFailure(PARSE_ERROR);
                    } finally {
                        getRoomsListener.onEnd();
                    }
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                if (getRoomsListener != null) {
                    listener.onFailure(CONNECTION_ERROR);
                }
            }
        });
    }

    public void getMembers(String roomId, OnGetObjectListener<ArrayList<RoomMember>> listener) {
        this.getRoomMembersListener = listener;

        getQuery.getValue(ROOMS_REFERENCE + roomId + "/members", new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                if (getRoomMembersListener != null) {
                    try {
                        ArrayList<RoomMember> members = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            RoomMember roomMember = parseRoomMember(snapshot);
                            members.add(roomMember);
                        }
                        getRoomMembersListener.onSuccess(members);
                    } catch (ParseException e) {
                        getRoomMembersListener.onFailure(PARSE_ERROR);
                    } finally {
                        getRoomMembersListener.onEnd();
                    }
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                if (getRoomMembersListener != null) {
                    listener.onFailure(CONNECTION_ERROR);
                }
            }
        });
    }

    private Room parseRoom(DataSnapshot snapshot) throws ParseException {
        return snapshot.getValue(Room.class);
    }

    private RoomMember parseRoomMember(DataSnapshot snapshot) throws ParseException {
        return snapshot.getValue(RoomMember.class);
    }

}
