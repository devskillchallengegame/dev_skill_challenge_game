package pl.base.application.cloud;

public enum ErrorStatus {
    CONNECTION_ERROR,
    PARSE_ERROR,
    UNKNOWN_ERROR
}