package pl.base.application.cloud.users;

import com.google.firebase.database.DataSnapshot;

import java.text.ParseException;
import java.util.ArrayList;

import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.User;
import pl.base.firebase_engine.database.FailureStatus;
import pl.base.firebase_engine.database.GetQuery;
import pl.base.firebase_engine.database.interfaces.OnGetValueListener;

import static pl.base.application.cloud.ErrorStatus.CONNECTION_ERROR;
import static pl.base.application.cloud.ErrorStatus.PARSE_ERROR;
import static pl.base.application.configs.ConnectionConfig.USERS_REFERENCE;

public class UserServant {

    private GetQuery getQuery;

    public UserServant(boolean cacheData) {
        getQuery = new GetQuery(cacheData);
    }

    public void getUser(String userId, OnGetObjectListener<User> listener) {
        getQuery.getValue(USERS_REFERENCE + userId, new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                try {
                    User user = parseUser(dataSnapshot);
                    listener.onSuccess(user);
                } catch (ParseException e) {
                    listener.onFailure(PARSE_ERROR);
                } finally {
                    listener.onEnd();
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                listener.onFailure(CONNECTION_ERROR);
            }
        });
    }

    public void getUsers(OnGetObjectListener<ArrayList<User>> listener) {
        getQuery.getValue(USERS_REFERENCE, new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                try {
                    ArrayList<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = parseUser(snapshot);
                        users.add(user);
                    }
                    listener.onSuccess(users);
                } catch (ParseException e) {
                    listener.onFailure(PARSE_ERROR);
                } finally {
                    listener.onEnd();
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                listener.onFailure(CONNECTION_ERROR);
            }
        });
    }

    public void getUsers(ArrayList<String> usersIdList, OnGetObjectListener<ArrayList<User>> listener) {
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < usersIdList.size(); i++) {
            String userId = usersIdList.get(i);
            boolean isLast = i == usersIdList.size() - 1;
            getQuery.getValue(USERS_REFERENCE + userId + "/", new OnGetValueListener() {
                @Override
                public void onSuccess(DataSnapshot dataSnapshot) {
                    try {
                        User user = parseUser(dataSnapshot);
                        users.add(user);
                        if (isLast) {
                            listener.onSuccess(users);
                            listener.onEnd();
                        }
                    } catch (ParseException e) {
                        listener.onFailure(PARSE_ERROR);
                    }
                }

                @Override
                public void onFailure(FailureStatus failureStatus) {
                    listener.onFailure(CONNECTION_ERROR);
                }
            });
        }
    }

    private User parseUser(DataSnapshot snapshot) throws ParseException {
        return snapshot.getValue(User.class);
    }

}
