package pl.base.application.cloud.rooms;

import android.support.annotation.Nullable;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.OnSentObjectListener;
import pl.base.application.models.RoomMember;
import pl.base.application.models.RoomStatus;
import pl.base.firebase_engine.auth.AuthManager;
import pl.base.firebase_engine.database.DeleteQuery;
import pl.base.firebase_engine.database.FailureStatus;
import pl.base.firebase_engine.database.PutQuery;
import pl.base.firebase_engine.database.interfaces.OnDeleteValueListener;

import static pl.base.application.configs.ConnectionConfig.ROOMS_REFERENCE;

@AllArgsConstructor
public class RoomUpdater {

    private String roomId;

    public void addMember(String userId, String userName, @Nullable OnSentObjectListener listener) {
        String reference = ROOMS_REFERENCE + roomId + "/members/" + userId;
        Long timeCreated = Calendar.getInstance().getTimeInMillis();
        RoomMember roomMember = new RoomMember(userId, userName, timeCreated, 0, false);
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(reference, roomMember, isSuccess -> {
            if (listener != null) {
                listener.onResult(isSuccess);
            }
        });
    }

    public void updateMemberParameters(int points, boolean hasFinished, @Nullable OnSentObjectListener listener) {
        String userId = AuthManager.getInstance().getUserId();
        String memberReference = ROOMS_REFERENCE + roomId + "/members/" + userId;
        String pointsReference = memberReference + "/points/";
        String statusReference = memberReference + "/hasFinished/";
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(pointsReference, points, pointsUpdated -> {
            if (pointsUpdated) {
                putQuery.putValue(statusReference, hasFinished, isSuccess -> {
                    if (listener != null) {
                        listener.onResult(isSuccess);
                    }
                });
            } else if (listener != null) {
                listener.onResult(false);
            }
        });
    }

    public void removeMember(String memberId, @Nullable OnDeleteValueListener listener) {
        String reference = ROOMS_REFERENCE + roomId + "/members/" + memberId;
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.deleteValue(reference, new OnDeleteValueListener() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                if (listener != null) {
                    listener.onFailure(failureStatus);
                }
            }
        });
    }

    public void changeRoomStatus(RoomStatus roomStatus, @Nullable OnSentObjectListener listener) {
        String reference = ROOMS_REFERENCE + roomId + "/status/";
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(reference, roomStatus, isSuccess -> {
            if (listener != null) {
                listener.onResult(isSuccess);
            }
        });
    }
}
