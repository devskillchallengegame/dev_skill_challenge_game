package pl.base.application.cloud.rooms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import pl.base.application.cloud.OnSentObjectListener;
import pl.base.application.models.DifficultyLevel;
import pl.base.application.models.Question;
import pl.base.application.models.Room;
import pl.base.application.models.RoomMember;
import pl.base.application.models.RoomStatus;
import pl.base.firebase_engine.auth.AuthManager;
import pl.base.firebase_engine.database.PutQuery;

import static pl.base.application.configs.ConnectionConfig.ROOMS_REFERENCE;
import static pl.base.application.utils.RandomGenerator.getRandomString;

@AllArgsConstructor
public class RoomCreator {

    public Room create(String name, int maxMembers, int questionCount, int timeToAnswer, DifficultyLevel level, ArrayList<Question> questions) {
        String randomId = getRandomString();
        String userId = AuthManager.getInstance().getUserId();
        Long timeCreated = Calendar.getInstance().getTimeInMillis();
        String userName = AuthManager.getInstance().getUserName();
        HashMap<String, RoomMember> members = new HashMap<>();
        members.put(userId, new RoomMember(userId, userName, timeCreated, 0, false));
        return Room.builder()
                .name(name)
                .maxMembers(maxMembers)
                .questions(questions)
                .questionsCount(questionCount)
                .timeToAnswer(timeToAnswer)
                .difficulty(level)
                .id(randomId)
                .ownerId(userId)
                .timeCreated(timeCreated)
                .members(members)
                .status(RoomStatus.NEW)
                .build();
    }

    public void send(Room room, OnSentObjectListener listener) {
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(ROOMS_REFERENCE + room.getId(), room, listener::onResult);
    }

}
