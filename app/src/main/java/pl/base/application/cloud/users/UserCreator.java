package pl.base.application.cloud.users;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.base.application.cloud.OnSentObjectListener;
import pl.base.application.models.User;
import pl.base.firebase_engine.auth.AuthManager;
import pl.base.firebase_engine.database.PutQuery;

import static pl.base.application.configs.ConnectionConfig.USERS_REFERENCE;

@AllArgsConstructor
@NoArgsConstructor
public class UserCreator {

    private OnSentObjectListener listener;

    public void create(String userId, String userName) {
        long currentTime = System.currentTimeMillis();
        User user = new User(userId, userName, 0, currentTime);
        AuthManager.getInstance().setUserName(userName, isSuccess -> {
            if (isSuccess) {
                send(user);
            } else {
                listener.onResult(false);
            }
        });
    }

    private void send(User user) {
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(USERS_REFERENCE + user.getId(), user, isSuccess -> {
            if (listener != null) {
                listener.onResult(isSuccess);
            }
        });
    }
}