package pl.base.application.cloud.questions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.base.application.cloud.OnSentObjectListener;
import pl.base.application.models.FirebaseQuestion;
import pl.base.firebase_engine.database.PutQuery;

import static pl.base.application.configs.ConnectionConfig.QUESTIONS_REFERENCE;

@NoArgsConstructor
@AllArgsConstructor
public class QuestionCreator {

    private OnSentObjectListener listener;

    public void create(FirebaseQuestion question) {
        send(question);
    }

    private void send(FirebaseQuestion question) {
        String reference = getReferenceForQuestion(question);
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(reference, question, isSuccess -> {
            if (listener != null) {
                listener.onResult(isSuccess);
            }
        });
    }

    private String getReferenceForQuestion(FirebaseQuestion question) {
        String reference = QUESTIONS_REFERENCE;
        reference += question.getCategory() + "/";
//        reference += question.getSubCategory() + "/";
//        reference += question.getDifficultyLevel().toString().toLowerCase() + "/";
        reference += question.getId() + "/";
        return reference;
    }

}
