package pl.base.application.cloud;

public interface OnGetObjectListener<T> {

    void onSuccess(T response);

    void onFailure(ErrorStatus errorStatus);

    void onEnd();

}
