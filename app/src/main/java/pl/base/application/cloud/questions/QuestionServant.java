package pl.base.application.cloud.questions;

import com.google.firebase.database.DataSnapshot;

import java.text.ParseException;
import java.util.ArrayList;

import pl.base.application.cloud.OnGetObjectListener;
import pl.base.application.models.FirebaseQuestion;
import pl.base.application.models.Question;
import pl.base.application.models.QuestionCategory;
import pl.base.firebase_engine.database.FailureStatus;
import pl.base.firebase_engine.database.GetQuery;
import pl.base.firebase_engine.database.interfaces.OnGetValueListener;

import static android.text.TextUtils.isEmpty;
import static pl.base.application.cloud.ErrorStatus.CONNECTION_ERROR;
import static pl.base.application.cloud.ErrorStatus.PARSE_ERROR;
import static pl.base.application.configs.ConnectionConfig.QUESTIONS_REFERENCE;

public class QuestionServant {

    private GetQuery getQuery;

    public QuestionServant() {
        getQuery = new GetQuery(true);
    }

    public void getQuestions(/*DifficultyLevel difficultyLevel, */QuestionCategory questionCategory, /*String subcategory,*/ OnGetObjectListener<ArrayList<Question>> listener) {
        String category = questionCategory.toString().toLowerCase();
        String reference = getQuestionsReference(/*difficultyLevel, */category/*, subcategory*/);
        getQuery.getValue(reference, new OnGetValueListener() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                try {
                    ArrayList<Question> questions = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        FirebaseQuestion question = parseQuestion(snapshot);
                        questions.add(question);
                    }
                    listener.onSuccess(questions);
                } catch (ParseException e) {
                    listener.onFailure(PARSE_ERROR);
                } finally {
                    listener.onEnd();
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                listener.onFailure(CONNECTION_ERROR);
            }
        });
    }

    // FIXME: 20.01.2018 handle subcategories and difficulty
    private String getQuestionsReference(/*DifficultyLevel difficultyLevel, */String category/*, String subcategory*/) {
        String reference = QUESTIONS_REFERENCE;
        reference = !isEmpty(category) ? reference + category + "/" : reference;
//        reference = !isEmpty(subcategory) ? reference + subcategory + "/" : reference;
//        reference = difficultyLevel != null ? reference + difficultyLevel.toString().toLowerCase() + "/" : reference;
        return reference;
    }

    private FirebaseQuestion parseQuestion(DataSnapshot snapshot) throws ParseException {
        return snapshot.getValue(FirebaseQuestion.class);
    }

}