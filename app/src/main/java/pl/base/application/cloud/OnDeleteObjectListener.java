package pl.base.application.cloud;

public interface OnDeleteObjectListener {

    void onSuccess();

    void onFailure(ErrorStatus errorStatus);

    void onEnd();
}