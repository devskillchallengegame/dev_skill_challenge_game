package pl.base.application.cloud.users;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.base.application.cloud.OnSentObjectListener;
import pl.base.firebase_engine.auth.AuthManager;
import pl.base.firebase_engine.database.PutQuery;

import static pl.base.application.configs.ConnectionConfig.USERS_REFERENCE;

@NoArgsConstructor
@AllArgsConstructor
public class UserUpdater {

    private OnSentObjectListener listener;

    public void update(String newUserName) {
        String userId = AuthManager.getInstance().getUserId();
        PutQuery putQuery = new PutQuery();
        putQuery.putValue(USERS_REFERENCE + userId + "/nick", newUserName, isPutValueSuccess -> {
            if (listener != null) {
                if (isPutValueSuccess) {
                    AuthManager.getInstance().setUserName(newUserName, isUpdateAuthSuccess -> {
                        listener.onResult(isUpdateAuthSuccess);
                    });
                } else {
                    listener.onResult(false);
                }
            }
        });
    }
}