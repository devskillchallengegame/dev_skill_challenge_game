package pl.base.application.cloud.rooms;

import android.support.annotation.Nullable;

import java.util.ArrayList;

import pl.base.application.cloud.ErrorStatus;
import pl.base.application.cloud.OnDeleteObjectListener;
import pl.base.firebase_engine.database.DeleteQuery;
import pl.base.firebase_engine.database.FailureStatus;
import pl.base.firebase_engine.database.interfaces.OnDeleteValueListener;

import static pl.base.application.cloud.ErrorStatus.CONNECTION_ERROR;
import static pl.base.application.configs.ConnectionConfig.ROOMS_REFERENCE;

public class RoomDestructor {

    public void delete(String roomId, @Nullable OnDeleteObjectListener listener) {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.deleteValue(ROOMS_REFERENCE + roomId, new OnDeleteValueListener() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                    listener.onEnd();
                }
            }

            @Override
            public void onFailure(FailureStatus failureStatus) {
                if (listener != null) {
                    listener.onFailure(CONNECTION_ERROR);
                    listener.onEnd();
                }
            }
        });
    }

    public void delete(ArrayList<String> roomsIdList, @Nullable OnDeleteObjectListener listener) {
        int listSize = roomsIdList.size();
        for (int i = 0; i < listSize; i++) {
            boolean isLast = i == listSize - 1;
            delete(roomsIdList.get(i), new OnDeleteObjectListener() {
                @Override
                public void onSuccess() {
                    if (isLast && listener != null) {
                        listener.onSuccess();
                        listener.onEnd();
                    }
                }

                @Override
                public void onFailure(ErrorStatus errorStatus) {
                    //TODO handle this
                }

                @Override
                public void onEnd() {

                }
            });
        }
    }

}
