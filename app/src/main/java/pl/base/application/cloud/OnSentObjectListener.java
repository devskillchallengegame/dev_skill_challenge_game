package pl.base.application.cloud;

public interface OnSentObjectListener {

    void onResult(boolean isSuccess);

}
