package pl.base.application.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pl.base.application.utils.SoundsUtils;

public class ScreenOffReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SoundsUtils.stopAllMusic();
    }
}
