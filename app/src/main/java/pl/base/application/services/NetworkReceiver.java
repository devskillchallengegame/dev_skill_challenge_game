package pl.base.application.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import pl.base.application.ui.screens.mainScreen.MainActivity;
import pl.base.application.ui.screens.splashScreen.SplashActivity;

import static pl.base.application.utils.ConnectivityUtils.isNetworkAvailable;

public class NetworkReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isNetworkAvailable(context) && !SplashActivity.isActivityOnTop()) {
            Toast.makeText(context, "Rozlączono z siecią", Toast.LENGTH_LONG).show();
            Intent startActivityIntent = new Intent(context, MainActivity.class);
            startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startActivityIntent);
        }
    }
}
