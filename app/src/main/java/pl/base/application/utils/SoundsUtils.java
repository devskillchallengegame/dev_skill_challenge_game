package pl.base.application.utils;

import android.media.MediaPlayer;

import pl.base.basic.R;

import static pl.base.application.configs.PrefsConfig.SOUND_PREFS_KEY;
import static pl.base.application.core.MyApplication.getAppContext;
import static pl.base.application.core.MyApplication.getGameConfig;
import static pl.base.application.core.MyApplication.getPrefsCore;

public class SoundsUtils {

    private static MediaPlayer backgroundPlayer;
    private static MediaPlayer challangePlayer;

    public static boolean isSoundOn() {
        return (boolean) getPrefsCore().getValue(SOUND_PREFS_KEY, getGameConfig().playMusicOnDefault());
    }

    public static void stopMusic() {
        stopBackgroundMusic();
        stopChallangeMusic();
    }

    public static void stopAllMusic() {
        if (backgroundPlayer != null) {
            backgroundPlayer.stop();
        }
        if (challangePlayer != null) {
            challangePlayer.stop();
        }
    }

    public static void playClickSound() {
        MediaPlayer mediaPlayer = MediaPlayer.create(getAppContext(), R.raw.click);
        mediaPlayer.start();
    }

    public static void playBackgroundMusic() {
        if (!isBackgroundMusicPlaying()) {
            backgroundPlayer = MediaPlayer.create(getAppContext(), R.raw.icytower);
            backgroundPlayer.setLooping(true);
            backgroundPlayer.start();
        }
    }

    public static void stopBackgroundMusic() {
        if (backgroundPlayer != null) {
            backgroundPlayer.stop();
        }
    }

    public static void playChallengeMusic() {
        if (!isChallangeMusicPlaying()) {
            challangePlayer = MediaPlayer.create(getAppContext(), R.raw.challange);
            challangePlayer.setLooping(true);
            challangePlayer.start();
        }
    }

    public static void stopChallangeMusic() {
        if (challangePlayer != null) {
            challangePlayer.stop();
        }
    }

    private static boolean isBackgroundMusicPlaying() {
        return backgroundPlayer != null && backgroundPlayer.isPlaying();
    }

    private static boolean isChallangeMusicPlaying() {
        return challangePlayer != null && challangePlayer.isPlaying();
    }

}
