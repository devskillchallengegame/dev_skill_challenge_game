package pl.base.application.utils;

import java.util.ArrayList;

import pl.base.application.models.Answer;
import pl.base.application.models.DifficultyLevel;
import pl.base.application.models.FirebaseQuestion;

public class FirebaseQuestionParser {

    public FirebaseQuestion parseQuestion(String rawQuestion) {
        String[] split = rawQuestion.split("/#");
        int splitLength = split.length;

        String questionId = split[0];
        String category = split[1];
        String subCategory = split[2];
        DifficultyLevel difficultyLevel = DifficultyLevel.valueOf(split[3].toUpperCase());
        String questionText = split[4];

        ArrayList<Answer> answers = new ArrayList<>();

        for (int i = 5, lp = 1; i < splitLength; i += 2, lp++) {
            String answerText = split[i];
            boolean isCorrectAnswer = Boolean.valueOf(split[i + 1]);
            answers.add(new Answer(lp, answerText, isCorrectAnswer));
        }

        return new FirebaseQuestion(questionId, category, subCategory, difficultyLevel, questionText, answers);
    }

}
