package pl.base.application.utils;

import java.util.ArrayList;
import java.util.HashMap;

import pl.base.application.models.RoomMember;

public class CollectionsUtils {

    public static ArrayList<RoomMember> getListFromSet(HashMap<String, RoomMember> hashMap) {
        ArrayList<RoomMember> values = new ArrayList<>();
        for (String id : hashMap.keySet()) {
            values.add(hashMap.get(id));
        }
        return values;
    }

}
