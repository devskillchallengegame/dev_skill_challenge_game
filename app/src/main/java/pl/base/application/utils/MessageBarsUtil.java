package pl.base.application.utils;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

public class MessageBarsUtil {

    public static void showToast(Activity activity, String text) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Activity activity, int textId) {
        showToast(activity, activity.getString(textId));
    }

    public static void showSnackBar(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }

    public static void showSnackBar(View view, int textId) {
        String text = view.getContext().getString(textId);
        showSnackBar(view, text);
    }

}
