package pl.base.application.utils;

import android.os.Handler;
import android.view.View;

import static pl.base.application.utils.SoundsUtils.playClickSound;

public abstract class OnOneClickListener implements View.OnClickListener {

    private static final int TIME_TO_BLOCK_IN_MILLIS = 1000;
    private boolean clickable = true;

    public abstract void onOneClick(View v);

    @Override
    public void onClick(View v) {
        if (clickable) {
            playClickSound();
            blockButton();
            onOneClick(v);
        }
    }

    private void blockButton() {
        clickable = false;
        final Runnable runnable = () -> clickable = true;
        Handler handler = new Handler();
        handler.postDelayed(runnable, TIME_TO_BLOCK_IN_MILLIS);
    }
}
