package pl.base.application.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import pl.base.basic.R;

public class DialogsUtils {

    private Context context;
    private static MaterialDialog progressDialog = null;

    public DialogsUtils(Context context) {
        this.context = context;
    }


    public void showInfoDialog(int titleTextId, int contentTextId) {
        showInfoDialog(titleTextId, contentTextId);
    }

    public void showProgressDialog(String title, String text) {
        showProgressDialog(context, title, text);
    }

    public void showProgressDialog(int resourceIdTitle, int resourceIdText) {
        showProgressDialog(context,
                context.getResources().getString(resourceIdTitle),
                context.getResources().getString(resourceIdText));
    }

    public void hideProgressDialogLocal() {
        hideProgressDialog();
    }

    public void showDefaultProgressDialog() {
        showDefaultProgressDialog(context);
    }


    public static void showProgressDialog(Context context, String title, String text) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        progressDialog = new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .theme(Theme.LIGHT)
                .progress(true, 0)
                .cancelable(false)
                .build();
        try {
            progressDialog.show();
        } catch (Exception e) {
            Logs.e(e);
        }
    }

    public static void showProgressDialog(Context context, int resourceIdTitle, int resourceIdText) {
        showProgressDialog(context, context.getResources().getString(resourceIdTitle), context.getResources().getString(resourceIdText));
    }

    public static void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static void showDefaultProgressDialog(Context context) {
        showProgressDialog(context, R.string.default_progress_dialog_title, R.string.default_progress_dialog_content);
    }

}