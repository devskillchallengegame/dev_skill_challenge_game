package pl.base.application.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ToggleButton;

import pl.base.basic.R;

import static android.support.v4.content.ContextCompat.getDrawable;

public class TouchableButton extends ToggleButton {

    private Context context;

    public TouchableButton(Context context) {
        super(context);
        this.context = context;
    }

    public TouchableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public TouchableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @Override
    public boolean performClick() {
        boolean isClicked = super.performClick();
        changeButtonHighlighting(this);
        return isClicked;
    }

    @Override
    @Deprecated
    public void setChecked(boolean checked) {
        super.setChecked(checked);
    }

    public TouchableButton setChecked() {
        super.setChecked(true);
        changeButtonHighlighting(this);
        return this;
    }

    public TouchableButton setUnchecked() {
        super.setChecked(false);
        changeButtonHighlighting(this);
        return this;
    }

    @Override
    public boolean isChecked() {
        return super.isChecked();
    }

    private void changeButtonHighlighting(ToggleButton button) {
        int btnHighlightingId = isChecked() ? R.drawable.btn_highlight : R.drawable.btn_no_highlight;
        Drawable drawable = getDrawable(context, btnHighlightingId);
        button.setBackgroundDrawable(drawable);
    }
}
