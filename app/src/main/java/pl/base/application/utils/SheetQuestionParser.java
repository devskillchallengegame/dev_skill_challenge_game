package pl.base.application.utils;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import pl.base.application.models.Answer;
import pl.base.application.models.SheetQuestion;

public class SheetQuestionParser {

    public SheetQuestion parseQuestion(String rawQuestion) {
        String[] split = rawQuestion.split("/#");
        ArrayList<Answer> answers = new ArrayList<>();
        answers.add(createAnswer(split[1]));
        return new SheetQuestion(split[0], answers);
    }

    @NonNull
    private Answer createAnswer(String text) {
        return new Answer(0, text, true);
    }

}
