package pl.base.application.utils;

import static java.util.UUID.randomUUID;

public class RandomGenerator {

    public static String getRandomString() {
        return randomUUID().toString();
    }

}
