package pl.base.application.utils;

import android.util.Log;

import static pl.base.application.core.MyApplication.LOGS_ENABLED;

public class Logs {

    public static void i(String message) {
        if (LOGS_ENABLED) Log.i("DEV_CHALLANGE", message);
    }

    public static void d(String message) {
        if (LOGS_ENABLED) Log.d("DEV_CHALLANGE", message);
    }

    public static void e(String message) {
        if (LOGS_ENABLED) Log.e("DEV_CHALLANGE", message);
    }

    public static void i(String tag, String message) {
        if (LOGS_ENABLED) Log.i(tag, message);
    }

    public static void d(String tag, String message) {
        if (LOGS_ENABLED) Log.d(tag, message);
    }

    public static void e(String tag, String message) {
        if (LOGS_ENABLED) Log.e(tag, message);
    }

    public static void e(Exception e) {
        e(Log.getStackTraceString(e));
    }
}
