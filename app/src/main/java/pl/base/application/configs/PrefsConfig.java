package pl.base.application.configs;

public class PrefsConfig {

    public static final String PREFS_NAME = "main_prefs";
    public static final String SOUND_PREFS_KEY = "sound_prefs_key";
    public static final String LAST_ROOM_NAME_KEY = "last_room_name_key";

}
