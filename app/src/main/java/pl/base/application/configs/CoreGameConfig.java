package pl.base.application.configs;

abstract class CoreGameConfig {

    public boolean isAdmin() {
        return false;
    }

    public long getRoomValidTime() {
        return 1200000;
    }

    public boolean isPvpMode() {
        return true;
    }

    public boolean playMusicOnDefault() {
        return true;
    }

    public boolean isAnswerFlashCard() {
        return false;
    }

    public String getSpreadSheetId() {
        return "1DD1wxhJlK8vvdWmN8ayZlg1W0EAleWB-GR1Y3gRwwYc";
    }

    public String getSpreadSheetApiKey() {
        return "AIzaSyCPlCeIhYj5mceT2NukFMuAv5tXYLxo5Ck";
    }

}
