package pl.base.application.models;

public enum QuestionCategory {
    PROGRAMMING,
    ALGORITHMS,
    GRAPHICS,
    SECURITY,
    NETWORKS,
    DATABASES,
    WEB_TECHNOLOGIES
}
