package pl.base.application.models;

import java.io.Serializable;

public enum DifficultyLevel implements Serializable {
    EASY,
    MEDIUM,
    HARD
}