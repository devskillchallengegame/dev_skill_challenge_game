package pl.base.application.models;

import java.util.ArrayList;

public class SheetQuestion extends Question {
    public SheetQuestion(String questionText, ArrayList<Answer> answers) {
        super(questionText, answers);
    }
}
