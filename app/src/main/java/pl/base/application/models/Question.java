package pl.base.application.models;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class Question implements Serializable {
    private String questionText;
    private ArrayList<Answer> answers;
}
