package pl.base.application.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room implements Serializable {
    private String id;
    private String ownerId;
    private String name;
    private Long timeCreated;
    private int questionsCount;
    private int maxMembers;
    private int timeToAnswer;
    private DifficultyLevel difficulty;
    private HashMap<String, RoomMember> members;
    private ArrayList<Question> questions;
    private RoomStatus status;
}
