package pl.base.application.models;


import java.io.Serializable;

public enum RoomStatus implements Serializable {
    NEW,
    DURING,
    FINISHED
}