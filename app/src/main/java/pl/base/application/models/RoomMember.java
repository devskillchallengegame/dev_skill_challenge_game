package pl.base.application.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoomMember implements Serializable {
    private String id;
    private String userName;
    private long timeJoined;
    private int points;
    private boolean hasFinished = false;
}