package pl.base.application.models;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FirebaseQuestion extends Question implements Serializable {
    private String id;
    private String category;
    private String subCategory;
    private DifficultyLevel difficultyLevel;

    public FirebaseQuestion(String id, String category, String subCategory, DifficultyLevel difficultyLevel, String questionText, ArrayList<Answer> answers) {
        super(questionText, answers);
        this.id = id;
        this.category = category;
        this.subCategory = subCategory;
        this.difficultyLevel = difficultyLevel;
    }

}
