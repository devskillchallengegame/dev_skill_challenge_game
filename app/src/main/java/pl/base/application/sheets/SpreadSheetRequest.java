package pl.base.application.sheets;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SpreadSheetRequest {
    private String apiKey;
    private String spreadsheetId;
    private String columnsSeparator;
    private String sheetName;
    private String fromCell;
    private String toCell;
}
