package pl.base.application.sheets;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.base.application.utils.Logs;

public class SpreadsheetDataCollector extends AsyncTask<Void, Void, List<String>> {

    private Sheets mService;
    private SpreadSheetRequest requestDetails;
    private ResultCallback resultCallback;

    public SpreadsheetDataCollector(GoogleCredential credential, SpreadSheetRequest requestDetails, ResultCallback resultCallback) {
        this.requestDetails = requestDetails;
        this.resultCallback = resultCallback;
        mService = new Sheets.Builder(AndroidHttp.newCompatibleTransport(),
                JacksonFactory.getDefaultInstance(),
                credential)
                .build();
    }

    @Override
    protected List<String> doInBackground(Void... params) {
        try {
            return call();
        } catch (Exception e) {
            Logs.e(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<String> output) {
        resultCallback.onResult(output);
    }

    @NonNull
    private List<String> prepareResponse(List<List<Object>> rows) {
        List<String> results = new ArrayList<>();
        if (rows != null) {
            int columnsCount = calculateColumnsCount();
            for (List column : rows) {
                StringBuilder rowFormatted = new StringBuilder();
                for (int i = 0; i < columnsCount; i++) {
                    rowFormatted.append(column.get(i));
                    rowFormatted.append(requestDetails.getColumnsSeparator());
                }
                results.add(rowFormatted.toString());
            }
        }
        return results;
    }

    private List<String> call() throws IOException {
        String criteria = requestDetails.getSheetName() + "!" + requestDetails.getFromCell() + ":" + requestDetails.getToCell();
        Sheets.Spreadsheets.Values.Get getDataRequest = mService.spreadsheets()
                .values().get(requestDetails.getSpreadsheetId(), criteria);
        getDataRequest.setKey((requestDetails.getApiKey()));
        ValueRange valueRange = getDataRequest.execute();
        List<List<Object>> rawRowsList = valueRange.getValues();
        return prepareResponse(rawRowsList);
    }

    private int calculateColumnsCount() {
        return requestDetails.getToCell().charAt(0) - requestDetails.getFromCell().charAt(0) + 1;
    }
}
