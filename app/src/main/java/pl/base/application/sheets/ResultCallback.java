package pl.base.application.sheets;

import java.util.List;

public interface ResultCallback {
    void onResult(List<String> response);
}
