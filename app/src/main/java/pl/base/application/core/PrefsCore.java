package pl.base.application.core;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class PrefsCore {

    private SharedPreferences preferences;

    public void initPrefs(Context context, String prefsName) {
        if (preferences == null) {
            preferences = context.getSharedPreferences(prefsName, MODE_PRIVATE);
        }
    }

    public void putValue(String key, Object value) {
        SharedPreferences.Editor editor = preferences.edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        }
        editor.apply();
    }

    public Object getValue(String key, Object defaultValue) {
        Object value = defaultValue;
        if (defaultValue instanceof String) {
            value = preferences.getString(key, (String) defaultValue);
        } else if (defaultValue instanceof Boolean) {
            value = preferences.getBoolean(key, (Boolean) defaultValue);
        } else if (defaultValue instanceof Long) {
            value = preferences.getLong(key, (Long) defaultValue);
        } else if (defaultValue instanceof Integer) {
            value = preferences.getInt(key, (Integer) defaultValue);
        } else if (defaultValue instanceof Float) {
            value = preferences.getFloat(key, (Float) defaultValue);
        }
        return value;
    }

}
