package pl.base.application.core;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import pl.base.application.configs.GameConfig;
import pl.base.application.services.ScreenOffReceiver;

import static pl.base.application.configs.PrefsConfig.PREFS_NAME;

public class MyApplication extends Application {

    public static boolean LOGS_ENABLED = true;
    private static Context applicationContext;
    private static PrefsCore prefsCore;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        prefsCore = new PrefsCore();
        prefsCore.initPrefs(this, PREFS_NAME);
        startScreenOffReceiver();
    }

    public static GameConfig getGameConfig() {
        return new GameConfig();
    }

    public static Context getAppContext() {
        return applicationContext;
    }

    public static PrefsCore getPrefsCore() {
        return prefsCore;
    }

    private void startScreenOffReceiver() {
        ScreenOffReceiver screenOffReceiver = new ScreenOffReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        this.registerReceiver(screenOffReceiver, filter);
    }
}
