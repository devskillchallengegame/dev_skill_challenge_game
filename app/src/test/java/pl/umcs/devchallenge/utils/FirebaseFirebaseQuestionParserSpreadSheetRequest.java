package pl.base.application.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import pl.base.application.models.Answer;
import pl.base.application.models.FirebaseQuestion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pl.base.application.models.DifficultyLevel.EASY;

public class FirebaseFirebaseQuestionParserSpreadSheetRequest {

    private FirebaseQuestionParser firebaseQuestionParser;
    private final String QUESTION_MOCK = "1/#programming/#java/#easy/#Jakim językiem jest java?/#Wieloparadygmatowy/#true/#obiektowy/#true/#strukturalny/#true/#imperatywny/#true/#funkcyjny/#true";
    private final String QUESTION_MOCK2 = "2/#programming/#java/#easy/#Czy klasa może implementować kilka interfejsów?/#Tylko wtedy jak interfejsy roszerzają jeden wspólny interfejs/#false/#Tak/#true/#Nie/#false";


    @Before
    public void initTest() {
        firebaseQuestionParser = new FirebaseQuestionParser();
    }

    @Test
    public void test1() {
        FirebaseQuestion question = firebaseQuestionParser.parseQuestion(QUESTION_MOCK);
        assertEquals("1", question.getId());
        assertEquals("programming", question.getCategory());
        assertEquals("java", question.getSubCategory());
        assertEquals(EASY, question.getDifficultyLevel());
        assertEquals("Jakim językiem jest java?", question.getQuestionText());
        ArrayList<Answer> answers = question.getAnswers();
        int answerId = 0;
        for (Answer answer : answers) {
            assertEquals(false, answer.getText().isEmpty());
            assertTrue(answerId == answer.getId() - 1);
            answerId += 1;
        }
    }

    @Test
    public void test2() {
        FirebaseQuestion question = firebaseQuestionParser.parseQuestion(QUESTION_MOCK2);
        assertEquals("2", question.getId());
        assertEquals("programming", question.getCategory());
        assertEquals("java", question.getSubCategory());
        assertEquals(EASY, question.getDifficultyLevel());
        assertEquals("Czy klasa może implementować kilka interfejsów?", question.getQuestionText());
        ArrayList<Answer> answers = question.getAnswers();
        int answerId = 0;
        for (Answer answer : answers) {
            assertEquals(false, answer.getText().isEmpty());
            assertTrue(answerId == answer.getId() - 1);
            answerId += 1;
        }
    }

}