package pl.base.application.configs;

public class ConnectionConfig {
    public static final String MAIN_REFERENCE = "game_beta/";
    public static final String ROOMS_REFERENCE = MAIN_REFERENCE + "rooms/";
    public static final String USERS_REFERENCE = MAIN_REFERENCE + "users/";
    public static final String QUESTIONS_REFERENCE = MAIN_REFERENCE + "questions/";
}
