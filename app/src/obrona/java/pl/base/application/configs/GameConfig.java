package pl.base.application.configs;

public class GameConfig extends CoreGameConfig {

    @Override
    public boolean isPvpMode() {
        return false;
    }

    @Override
    public boolean isAnswerFlashCard() {
        return true;
    }
}
