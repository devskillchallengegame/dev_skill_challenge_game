package pl.base.firebase_engine.core;

public interface BaseListener {

    void onResult(boolean isSuccess);

}
