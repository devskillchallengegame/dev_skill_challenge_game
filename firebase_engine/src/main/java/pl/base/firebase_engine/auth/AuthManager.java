package pl.base.firebase_engine.auth;

import android.support.annotation.Nullable;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import pl.base.firebase_engine.auth.interfaces.SetUserNameListener;
import pl.base.firebase_engine.auth.interfaces.SignInListener;

public class AuthManager {

    private static AuthManager instance = null;
    private static FirebaseAuth firebaseAuth;

    private AuthManager() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public static AuthManager getInstance() {
        if (instance == null) {
            instance = new AuthManager();
        }
        return instance;
    }

    public void signInAnonymously(SignInListener listener) {
        firebaseAuth.signInAnonymously()
                .addOnCompleteListener(task -> listener.onResult(task.isSuccessful()));
    }

    public boolean signOut() {
        firebaseAuth.signOut();
        return !isSigned();
    }

    public boolean isSigned() {
        return getUser() != null;
    }

    public void setUserName(String userName, SetUserNameListener listener) {
        if (isSigned()) {
            UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                    .setDisplayName(userName)
                    .build();
            FirebaseUser user = getUser();
            user.updateProfile(profileChangeRequest)
                    .addOnCompleteListener(task -> listener.onResult(true));
        }
    }

    @Nullable
    public String getUserName() {
        String userName = null;
        if (isSigned()) {
            userName = getUser().getDisplayName();
        }
        return userName;
    }

    @Nullable
    public String getUserId() {
        String userId = null;
        if (isSigned()) {
            userId = getUser().getUid();
        }
        return userId;
    }

    @Nullable
    private FirebaseUser getUser() {
        return firebaseAuth.getCurrentUser();
    }

}
