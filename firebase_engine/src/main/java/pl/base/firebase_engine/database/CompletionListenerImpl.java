package pl.base.firebase_engine.database;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import lombok.AllArgsConstructor;
import pl.base.firebase_engine.database.interfaces.OnDeleteValueListener;

import static pl.base.firebase_engine.database.ReferenceHelper.getFailureStatus;

@AllArgsConstructor
class CompletionListenerImpl implements DatabaseReference.CompletionListener {

    private OnDeleteValueListener onDeleteValueListener;

    @Override
    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
        if (databaseError == null) {
            onDeleteValueListener.onSuccess();
        } else {
            onDeleteValueListener.onFailure(getFailureStatus(databaseError));
        }
    }
}
