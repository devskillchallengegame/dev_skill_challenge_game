package pl.base.firebase_engine.database.interfaces;

import pl.base.firebase_engine.database.FailureStatus;

public interface OnDeleteValueListener {
    void onSuccess();

    void onFailure(FailureStatus failureStatus);
}
