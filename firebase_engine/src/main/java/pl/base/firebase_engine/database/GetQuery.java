package pl.base.firebase_engine.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import lombok.AllArgsConstructor;
import pl.base.firebase_engine.database.interfaces.OnGetValueListener;

import static pl.base.firebase_engine.database.ReferenceHelper.findChildReference;

@AllArgsConstructor
public class GetQuery {

    private boolean cacheData;

    public void getValue(String reference, OnGetValueListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = findChildReference(database, reference);
        ValueEventListener valueEventListener = new ValueEventListenerImpl(listener);
        if (cacheData) {
            databaseReference.addValueEventListener(valueEventListener);
        } else {
            databaseReference.addListenerForSingleValueEvent(valueEventListener);
        }
    }

}
