package pl.base.firebase_engine.database;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.List;

import static com.google.firebase.database.DatabaseError.DISCONNECTED;
import static com.google.firebase.database.DatabaseError.EXPIRED_TOKEN;
import static com.google.firebase.database.DatabaseError.INVALID_TOKEN;
import static com.google.firebase.database.DatabaseError.MAX_RETRIES;
import static com.google.firebase.database.DatabaseError.NETWORK_ERROR;
import static com.google.firebase.database.DatabaseError.PERMISSION_DENIED;
import static pl.base.firebase_engine.database.FailureStatus.CONNECTION_LOST;
import static pl.base.firebase_engine.database.FailureStatus.INVALID_SESSION_TOKEN;
import static pl.base.firebase_engine.database.FailureStatus.LIMIT_REACHED;
import static pl.base.firebase_engine.database.FailureStatus.NO_PERMISSION;
import static pl.base.firebase_engine.database.FailureStatus.UNKNOWN_ERROR;

class ReferenceHelper {

    static DatabaseReference findChildReference(FirebaseDatabase database, String reference) {
        DatabaseReference databaseReference = database.getReference();
        String[] childsKeys = reference.split("/");
        List<String> childsKeysList = Arrays.asList(childsKeys);
        for (String childKey : childsKeysList) {
            databaseReference = databaseReference.child(childKey);
        }
        return databaseReference;
    }

    static FailureStatus getFailureStatus(DatabaseError error) {
        FailureStatus failureStatus = UNKNOWN_ERROR;
        switch (error.getCode()) {
            case DISCONNECTED | NETWORK_ERROR:
                failureStatus = CONNECTION_LOST;
                break;
            case MAX_RETRIES:
                failureStatus = LIMIT_REACHED;
                break;
            case INVALID_TOKEN | EXPIRED_TOKEN:
                failureStatus = INVALID_SESSION_TOKEN;
                break;
            case PERMISSION_DENIED:
                failureStatus = NO_PERMISSION;
        }
        return failureStatus;
    }

}
