package pl.base.firebase_engine.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import pl.base.firebase_engine.database.interfaces.OnDeleteValueListener;

import static pl.base.firebase_engine.database.ReferenceHelper.findChildReference;

public class DeleteQuery {

    public void deleteValue(String reference, OnDeleteValueListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = findChildReference(database, reference);
        CompletionListenerImpl onCompleteListener = new CompletionListenerImpl(listener);
        databaseReference.removeValue(onCompleteListener);
    }

}
