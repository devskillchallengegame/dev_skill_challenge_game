package pl.base.firebase_engine.database.interfaces;

import com.google.firebase.database.DataSnapshot;

import pl.base.firebase_engine.database.FailureStatus;

public interface OnGetValueListener {

    void onSuccess(DataSnapshot dataSnapshot);

    void onFailure(FailureStatus failureStatus);

}
