package pl.base.firebase_engine.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import lombok.AllArgsConstructor;
import pl.base.firebase_engine.database.interfaces.OnGetValueListener;

import static pl.base.firebase_engine.database.ReferenceHelper.getFailureStatus;

@AllArgsConstructor
class ValueEventListenerImpl implements ValueEventListener {

    private OnGetValueListener listener;

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        listener.onSuccess(dataSnapshot);
    }

    @Override
    public void onCancelled(DatabaseError error) {
        listener.onFailure(getFailureStatus(error));
    }
}
