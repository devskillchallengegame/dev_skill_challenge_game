package pl.base.firebase_engine.database;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import pl.base.firebase_engine.database.interfaces.OnPutValueListener;

import static pl.base.firebase_engine.database.ReferenceHelper.findChildReference;

public class PutQuery {

    public void putValue(String reference, Object object, OnPutValueListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = findChildReference(database, reference);
        databaseReference.setValue(object)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        listener.onResult(task.isSuccessful());
                    }
                });
    }

}
