package pl.base.firebase_engine.database;

public enum FailureStatus {
    CONNECTION_LOST,
    LIMIT_REACHED,
    INVALID_SESSION_TOKEN,
    NO_PERMISSION,
    UNKNOWN_ERROR
}
